/* const withPWA = require('next-pwa')({
  dest: 'public',
}) */

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  compiler: {
    // ssr and displayName are configured by default
    styledComponents: true,
  },
  images: {
    domains: [
      'lh3.googleusercontent.com',
      'playback-wholesale.s3.amazonaws.com',
      'media-exp1.licdn.com',
      'ucarecdn.com',
      'ca.slack-edge.com',
    ],
  },
  env: {
    API_URI: process.env.API_URI,
    SECRET_TOKEN: process.env.SECRET_TOKEN,
    SERVER_URI: process.env.SERVER_URI,
  },
}

module.exports = nextConfig
