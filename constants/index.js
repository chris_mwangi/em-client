// Theme states
export const LIGHT_THEME = 'light'
export const DARK_THEME = 'dark'

// User states
export const LOG_OUT = 'CLEAR_USER'
export const SET_USER = 'SET_USER'
export const SEARCH_PROJECT = 'SEARCH_PROJECT'
export const SEARCH_USER = 'SEARCH_USER'
export const SEARCH_MESSAGES = 'SEARCH_MESSAGES'

// Tab states
export const SET_TAB = 'SET_TAB'

// loading state
export const OPEN_LOADING = 'OPEN_LOADING'
export const CLOSE_LOADING = 'CLOSE_LOADING'
export const OPEN_HOME_LOADING = 'OPEN_HOME_LOADING'
export const CLOSE_HOME_LOADING = 'CLOSE_HOME_LOADING'

// error states
export const OPEN_ERROR = 'OPEN_ERROR'
export const CLOSE_ERROR = 'CLOSE_ERROR'

// menu
export const OPEN_MENU = 'OPEN_MENU'
export const CLOSE_MENU = 'CLOSE_MENU'
