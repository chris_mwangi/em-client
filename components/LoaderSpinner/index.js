/* eslint-disable react/jsx-no-useless-fragment */
// import Image from 'next/image'
import { useRef } from 'react'
import styled from 'styled-components'

const Background = styled.div`
  width: 100%;
  height: 100;
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-content: center;
  align-items: center;
  z-index: 9999;
`

const ModalWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyledSpinner = styled.svg`
  animation: rotate 1s linear infinite;
  width: 50px;
  height: 100px;

  & .path {
    stroke: #fff;
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }

  @keyframes rotate {
    100% {
      transform: rotate(360deg);
    }
  }
  @keyframes dash {
    0% {
      stroke-dasharray: 1, 150;
      stroke-dashoffset: 0;
    }
    50% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -35;
    }
    100% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -124;
    }
  }
`

function LoaderSpinner({ showModal }) {
  const modalRef = useRef()

  return (
    <>
      {showModal ? (
        <Background ref={modalRef}>
          <ModalWrapper showModal={showModal}>
            <StyledSpinner viewBox="0 0 50 50">
              <circle
                className="path"
                cx="25"
                cy="25"
                r="20"
                fill="none"
                strokeWidth="4"
              />
            </StyledSpinner>
            {/*  <Image height="130px" width="130px" src="/assets/mobile/gifs/yy3.gif" /> */}
          </ModalWrapper>
        </Background>
      ) : null}
    </>
  )
}

export default LoaderSpinner
