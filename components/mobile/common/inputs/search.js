import styled from 'styled-components'
import { useGlobalContext } from '../../../../context/provider'

const SearchWrapper = styled.div`
  display: flex;
  align-items: center;
  // background: ${({ theme }) => theme.colors.graySlight};
  background: ${({ theme }) => theme.colors.background};
  border-radius: 6px;
  // width: 80%;
  padding: 0 13px 0 10px;
  margin-left: 7px;
  // border: 1px solid ${(props) => props.theme.colors.graySlight};
`

const SearchSvg = styled.svg`
  width: 17.96px;
  height: 17.65px;
  color: ${({ theme }) => theme.colors.text};
`
const B = styled.div`
  border-left: 1px solid ${({ theme }) => theme.colors.primary};
  height: 21px;
  opacity: 0;
  // margin-left: 1rem;
`

const Search = styled.input`
  border: none;
  outline: none;
  background: ${({ theme }) => theme.colors.background};
  height: 32px;
  width: 100%;
  font-size: ${({ theme }) => theme.gutters.medium};
  color: ${({ theme }) => theme.colors.text};
`

const search = ({ type, placeholder = 'Search' }) => {
  const { searchDispatch, searchState } = useGlobalContext()

  return (
    <SearchWrapper>
      <SearchSvg>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
            clipRule="evenodd"
          />
        </svg>
      </SearchSvg>
      <B />
      <Search
        type="search"
        placeholder={placeholder}
        value={searchState.projectQueryString}
        onChange={(e) => searchDispatch({ type, payload: e.target.value })}
      />
    </SearchWrapper>
  )
}

export default search
