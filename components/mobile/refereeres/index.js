import styled from 'styled-components'
import TestimonialBox from './testimonialBox'

const Section = styled.section`
  margin-top: 63px;
`

const data = [
  {
    name: 'Kannan Reghu,,Founder & CTO at Playback',
    linkedinUrl: 'https://github.com/',
    profilPicUrl: 'https://ca.slack-edge.com/TAF02P92Q-UAG3NRZ0E-f48b922498b6-512',
    message:
      ' Museveni agreed to lift the suspension on the activities of DGF in Uganda. Sources tell us this was a charade as the fate of the facility had long been decided, with the conditions put',
  },
  {
    name: 'Ajesh Gopi,,UI/UX designer at Playback',
    linkedinUrl: 'https://github.com/',
    profilPicUrl: 'https://ca.slack-edge.com/TAF02P92Q-U0192D3TXN1-69b0fab80d35-512',
    message:
      ' Museveni agreed to lift the suspension on the activities of DGF in Uganda. Sources tell us this was a charade as the fate of the facility had long been decided, with the conditions put',
  },
  {
    name: 'Caxton Muthoni,,Software developer at BAT',
    linkedinUrl: 'https://github.com/',
    profilPicUrl:
      'https://media-exp1.licdn.com/dms/image/C5603AQHP7Tkxv4VtnQ/profile-displayphoto-shrink_400_400/0/1633883720822?e=1670457600&v=beta&t=N5uyvAzFwaI6gKMy2oTFz6Ygdl3-ZhnS_ga3RVlOLzs',
    message:
      ' Museveni agreed to lift the suspension on the activities of DGF in Uganda. Sources tell us this was a charade as the fate of the facility had long been decided, with the conditions put',
  },
  {
    name: 'Juan Chiu,,Frontend developer at Playback',
    linkedinUrl: 'https://github.com/',
    profilPicUrl: 'https://ca.slack-edge.com/TAF02P92Q-U01JJ4U9X16-gcd4f01893df-512',
    message:
      ' Museveni agreed to lift the suspension on the activities of DGF in Uganda. Sources tell us this was a charade as the fate of the facility had long been decided, with the conditions put',
  },
]
const index = () => {
  return (
    <Section>
      {data?.map((item) => (
        <TestimonialBox item={item} />
      ))}
    </Section>
  )
}

export default index
