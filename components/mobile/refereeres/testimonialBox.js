import Image from 'next/image'
import React from 'react'
import styled from 'styled-components'
import { WrapperBox } from '../about'

const Wrapper = styled.div`
  padding: 0.4rem 0;
`
const Profile = styled.div`
  display: flex;
  align-items: center;
  column-gap: 13px;
  span {
    font-family: ${({ theme }) => theme.fonts.NunitoBold};
    font-size: ${({ theme }) => theme.gutters.medium};
    color: ${({ theme }) => theme.colors.text};
  }
`
const Body = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.reqular};
  /* span::before {
    font-family: 'Nunito-SemiBold';
    content: '“';
    font-size: 58px;
    color: #ffb2;
    position: absolute;
    padding-right: 2.8rem;
    width: 3rem;
    left: 10px;
    top: -1.65rem;
  }
  strong::after {
    font-family: 'Nunito-SemiBold';
    content: '“';
    font-size: 50px;
    color: #ffb2d5;
    position: absolute;
    transform: rotate(180deg);
    bottom: -19px;
  } */
`
const P = styled.p`
  font-family: 'Nunito-Regular';
  font-size: 15px;
  line-height: ${({ theme }) => theme.gutters.xlarge};
`
const Img = styled(Image)`
  border-radius: 50%;
`

function TestimonialBox({ item }) {
  return (
    <WrapperBox>
      <Wrapper>
        <Profile>
          <Img src={item?.profilPicUrl} width={35} height={35} alt="" />
          <span>{item?.name}</span>
        </Profile>
        <Body>
          <span />
          <P>{item?.message}</P>
          <strong />
        </Body>
      </Wrapper>
    </WrapperBox>
  )
}

export default TestimonialBox
