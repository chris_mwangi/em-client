import Image from 'next/image'

function ImageContainer({ src, height, width }) {
  return (
    <Image
      src={src}
      height={height}
      width={width}
      placeholder="blur"
      blurDataURL={src}
    />
  )
}

export default ImageContainer
