/* eslint-disable import/no-cycle */
import { useContext, useState } from 'react'
import { BiLinkExternal } from 'react-icons/bi'

import { AiFillGitlab } from 'react-icons/ai'
import styled, { ThemeContext } from 'styled-components'
import { WrapperBox, BoldTextRegular, NormalText } from './index'
import ProjectsCard from './projectsCard'
import { openInNewTab } from '../../../libs/helpers'

const Wr = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.colors.bg};
`

const ProjectName = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 600;
  font-size: 16px;
  line-height: calc(24 / 16);
  justify-self: flex-end;
  text-align: end;
  width: 100%;
`
const ProjectBoxWrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-gap: 8px;
  padding-bottom: 9px;
`
const ProjectBox = styled.div`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding: 7px 12px 15px 12px;
  background: ${({ theme }) => theme.colors.bg};
  margin-bottom: 2px;
`

const TechWrapper = styled.div`
  display: flex;
  column-gap: 12px;
  margin-top: 1rem;
  flex-wrap: wrap;
`

const Tech = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};

  -webkit-font-smoothing: antialiased;
  font-size: 15px;
  line-height: calc(24 / 16);
`

const Button = styled.button`
  outline: none;
  border: 1px solid ${({ theme }) => theme.colors.link};
  padding: 7px 17px;
  max-width: 40vw;
  justify-self: center;
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.Apple};
  background-color: transparent;
  color: ${({ theme }) => theme.colors.link};
  border-radius: 3px;
  margin: 19px 0;
`

function Projects() {
  const { gutters, colors } = useContext(ThemeContext)
  const [show, setShow] = useState(false)

  const projects = [
    {
      name: 'Playback Mail App',
      projectImage: 'https://ucarecdn.com/c3310d91-8852-4112-bba7-ccee58da85b3/',
      description:
        'Email Reimagined For Creators.Still running your creator business on your personal email? Get Playback, the only business email app built for content creators',
      techStack: [
        'React',
        'React Native',
        'Typescript',
        'Ruby on rails Graphql api',
        'Redux/Reduxtoolkit',
      ],
      webLink: 'https://tikti.vercel.app/',
      playStoreUrl:
        'https://play.google.com/store/apps/details?id=com.playback.creator',
      linkUrl: 'https://creator.letsplayback.com/',
    },
    {
      name: 'Playback Video Shopping',
      projectImage: 'https://ucarecdn.com/95b4ccc6-b618-4d28-be99-16563ae66d3a/',
      description:
        'The world’s best video shopping marketplace for indie brand.Instantly buy the products your favorite creators love.What you get is what you see – always shipped for FREE.Join the video shopping revolution.',
      techStack: [
        'Reactjs',
        'Nextjs',
        'Appolo client',
        'Nodejs/express graphql api',
        'Ruby microservices',
        'Docker + aws',
      ],
      webLink: 'https://www.letsplayback.com/',
      linkUrl: 'https://www.letsplayback.com/',
    },
    {
      name: 'Creator Brand dashboard',
      projectImage: 'https://ucarecdn.com/f347d299-028d-483e-8a58-8924feb4ef7f/',
      description:
        'Manage your brand partnerships  with organized way through simple tools and flow  ...',
      techStack: [
        'Reactjs',
        'Nextjs',
        'Appolo client',
        'Ruby on rails Graphql api',
        'Docker + aws',
        'Typescript',
      ],
      webLink: 'https://brand.letsplayback.com/',
      linkUrl: 'https://brand.letsplayback.com/',
    },
    {
      name: 'Better coach',
      projectImage: 'https://ucarecdn.com/15e3dedf-1f65-40c6-81d7-58302ded195f/',
      description:
        'Global coach poolChoose from our global pool of 1,000+ rigorously selected certified coaches, facilitators, and experts. We always develop a dedicated coach pool for our clients to match your organizational and coaching requirements.',
      techStack: [
        'Reactjs',
        'Typescript',
        'Appolo client',
        'Rails Graphql api',
        'Aws amplify',
      ],
      webLink: 'https://bettercoach.io/',
      linkUrl: 'https://bettercoach.io/',
    },

    /*   {
      name: 'Instagram Clone',
      description: 'Post your best photos and let the worls comment',
      tech: ['React', 'Nextjs', 'Firebase', 'Recoil', 'Google Auth', 'Tailwind css'],
      gitlabLink: '',
      webLink: '',
    }, */
  ]

  const otherProjects = [
    /*   {
      name: 'Instagram Clone',
      description: 'Post your best photos and let the worls comment',
      tech: ['React', 'Nextjs', 'Firebase', 'Recoil', 'Google Auth', 'Tailwind css'],
      gitlabLink: '',
      webLink: '',
    }, */
    {
      name: 'Atilead',
      description:
        'Best customer management tools. Manage your leads centrally. Send bulk and automated marketing mails,',
      tech: [
        'Vuejs',
        'Nodejs/Express',
        'Graphql',
        'Docker+Aws',
        'Google Auth',
        'scss',
      ],
      webLink: 'https://atilead.com/',
    },
    {
      name: 'Admin,Playback',
      description:
        'A dashboard to manage all the brands and creators flow.It has the ability of creating campaigns,connecting resellers and more',
      tech: [
        'React',
        'Nextjs',
        'Firebase',
        'Ruby on rails',
        'Google Auth',
        'styled components',
      ],
      // webLink: 'https://ops.letsplayback.com/',
    },
    {
      name: 'Tiktok Clone(For fun)',
      description:
        'Trends start here. On a device or on the web, viewers can watch and discover and upload personalized videos.',
      tech: [
        'React',
        'Nextjs',
        'Typescript',
        'Sanityio',
        'Google Auth',
        'Tailwind css',
      ],
      gitlabLink: 'https://gitlab.com/chris_mwangi/m.findke',
      webLink: 'https://mtikti.vercel.app/',
    },
    {
      name: 'Nodejs content writer',
      description:
        'Content on Express REST api not limited to auth flows and security.',
      tech: ['Nodejs', 'Graphcms', 'Rest', 'Google security'],
      webLink: 'https://nodejstutorials.net/node-js-rest-api-security-using-jwt/',
    },
    {
      name: 'Better coach',
      description:
        'Codebase review on the project.Solution for the (internal) coaching and workshops',
      tech: ['Ruby on rails api', 'Reactjs', 'Graphql', 'Typescript'],
      webLink: 'https://bettercoach.io/',
    },
  ]
  return (
    <Wr>
      <WrapperBox
        id="projects"
        initial={{ opacity: 0 }}
        transition={{ delay: 0.1 }}
        whileInView={{ opacity: 1 }}
        viewport={{ once: true }}
        style={{ marginBottom: '1rem' }}
      >
        <BoldTextRegular
          style={{ marginBottom: gutters.small, marginTop: gutters.tiny }}
        >
          Recent Relevent Projects
        </BoldTextRegular>
        {projects?.map((project) => (
          <ProjectsCard project={project} />
        ))}
        <BoldTextRegular
          style={{ textAlign: 'start', fontSize: '17px', marginBottom: '1rem' }}
        >
          Other noteworthy projects
        </BoldTextRegular>
        <ProjectBoxWrapper>
          {!show
            ? otherProjects.slice(0, 3)?.map((pr) => (
                <ProjectBox>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginBottom: '1rem',
                    }}
                  >
                    {pr?.gitlabLink ? (
                      <AiFillGitlab fontSize="21px" color={colors.white} />
                    ) : (
                      <div />
                    )}
                    {pr?.webLink && (
                      <BiLinkExternal
                        onClick={() => openInNewTab(pr?.webLink)}
                        fontSize="21px"
                        color={colors.white}
                      />
                    )}
                  </div>
                  <ProjectName style={{ textAlign: 'start', margin: 0 }}>
                    {pr?.name}
                  </ProjectName>

                  <NormalText>{pr?.description}</NormalText>
                  <TechWrapper>
                    {pr?.tech?.map((tech) => (
                      <Tech key={tech}>{tech}</Tech>
                    ))}
                  </TechWrapper>
                </ProjectBox>
              ))
            : otherProjects.map((pr) => (
                <ProjectBox>
                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      marginBottom: '1rem',
                    }}
                  >
                    {pr?.gitlabLink ? (
                      <AiFillGitlab fontSize="21px" color={colors.white} />
                    ) : (
                      <div />
                    )}
                    {pr?.webLink && (
                      <BiLinkExternal
                        onClick={() => openInNewTab(pr?.webLink)}
                        fontSize="21px"
                        color={colors.white}
                      />
                    )}
                  </div>
                  <ProjectName style={{ textAlign: 'start', margin: 0 }}>
                    {pr?.name}
                  </ProjectName>

                  <NormalText>{pr?.description}</NormalText>
                  <TechWrapper>
                    {pr?.tech?.map((tech) => (
                      <Tech key={tech}>{tech}</Tech>
                    ))}
                  </TechWrapper>
                </ProjectBox>
              ))}
          {show ? (
            <Button onClick={() => setShow(!show)}>Show less</Button>
          ) : (
            <Button onClick={() => setShow(!show)}>Show all</Button>
          )}
        </ProjectBoxWrapper>
      </WrapperBox>
    </Wr>
  )
}

export default Projects
