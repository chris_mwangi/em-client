/* eslint-disable import/no-cycle */
import { useContext, useEffect, useRef, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { Waypoint } from 'react-waypoint'
import { BsFillPlayFill } from 'react-icons/bs'
import { HiVolumeUp, HiVolumeOff } from 'react-icons/hi'
import { WrapperBox, NormalText, BoldTextRegular } from './index'

const VideoWrapper = styled.div`
  margin-top: 0.6rem;
  background: linear-gradient(
    0deg,
    rgba(242, 255, 246, 1) 0%,
    rgba(255, 241, 248, 1) 100%
  );
  height: 100%;
  width: 100vw;
  overflow: hidden;
  position: relative;

  .vimeo-wrapper {
    position: relative;
    margin: 0;
    margin-bottom: -0.3rem;
    p {
      position: absolute;
      right: ${({ theme }) => theme.gutters.large};
      top: ${({ theme }) => theme.gutters.tiny};
      font-family: 'Caveat', cursive;
      font-size: ${({ theme }) => theme.gutters.regular};
      color: #fff;
    }
    /*  p::before {
      content: '';
      width: 100%;
      height: 100%;
      display: block;
      position: absolute;
      background: linear-gradient(
        0deg,
        #ffffff 0%,
        rgba(255, 255, 255, 0.961265) 3.44%,
        rgba(255, 255, 255, 0.815448) 8.13%,
        rgba(255, 255, 255, 0.471765) 20.35%,
        rgba(255, 255, 255, 0) 35.25%
      );
    } */
  }
`
const Video = styled.video`
  width: 100%;
  height: 100%;
`
const Wrapper = styled.div``

const IconHolder = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  .icon {
    font-size: 73px;
    color: #fff;
  }
`
const IconHolderMute = styled.div`
  position: absolute;
  right: 5%;
  bottom: 7%;

  .icon {
    font-size: 25px;
    color: #fff;
  }
`

const currentWorkplace = ({ homeVideo }) => {
  const { gutters } = useContext(ThemeContext)
  const [playing, setPlaying] = useState(false)
  const [isVideoMuted, setIsVideoMuted] = useState(false)

  const videoRef = useRef(null)

  const handleExitViewport = () => {
    videoRef?.current?.pause()
    setPlaying(false)
  }

  const onVideoPress = () => {
    if (playing) {
      videoRef?.current?.pause()
      setPlaying(false)
    } else {
      videoRef?.current?.play()
      setPlaying(true)
    }
  }

  useEffect(() => {
    if (videoRef?.current) {
      videoRef.current.muted = isVideoMuted
    }
  }, [isVideoMuted])

  // console.log(homeVideo?.getHomeVideo?.video)
  return (
    <Wrapper>
      <WrapperBox>
        <BoldTextRegular
          style={{ marginBottom: gutters.small, marginTop: gutters.tiny }}
        >
          Current workplace
        </BoldTextRegular>
        <NormalText>
          Playback | Finally – a business email app that gets what creators do.Still
          running your creator business on your personal email? Get Playback, the
          only business email app built for content creators
        </NormalText>
      </WrapperBox>
      <Waypoint onLeave={handleExitViewport}>
        <VideoWrapper>
          <div className="vimeo-wrapper">
            <p>{homeVideo?.getHomeVideo?.video?.videoName}</p>
            <Video
              onClick={onVideoPress}
              loop
              playing={playing}
              ref={videoRef}
              id="video"
              src={homeVideo?.getHomeVideo?.video?.videoUrl}
            />
          </div>
          <IconHolder onClick={onVideoPress}>
            <div>
              {!playing && (
                <span>
                  <BsFillPlayFill className="icon" />
                </span>
              )}
            </div>
          </IconHolder>
          <IconHolderMute onClick={() => setIsVideoMuted(!isVideoMuted)}>
            {isVideoMuted ? (
              <span>
                <HiVolumeOff className="icon" />
              </span>
            ) : (
              <span>
                <HiVolumeUp className="icon" />
              </span>
            )}
          </IconHolderMute>
        </VideoWrapper>
      </Waypoint>
    </Wrapper>
  )
}

export default currentWorkplace
