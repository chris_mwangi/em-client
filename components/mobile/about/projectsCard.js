// const widgetUrl = "http://localhost:3000";
const widgetUrl = "https://playback-interactive-widget-app-mu.vercel.app/";
const API_BASE_URL = "https://apiintr.letsplayback.com";
const PROTOCOL = "https://";
const API_X_PQ_APP =
  "eyJhbGciOiJub25lIn0.eyJkYXRhIjoiNjNhYzVhYWVlMGQzZWQyNjU1MTkxNTEzIn0.";

/// Apis header Api key/etc
const HEADERS = {
  "Content-Type": "application/json",
  Accept: "application/json",
  "X-PQ-App": API_X_PQ_APP,
};

const scripts = document.getElementsByTagName("script");
const jsFile = new URL(
  PROTOCOL + scripts[scripts.length - 1].attributes.src.value
);
const pid = jsFile.searchParams.get("pid");
const prv = jsFile.searchParams.get("prv");

window.addEventListener("load", async (e) => {
  const fetchAPIS = async () => {
    try {
      // Send a GET request
      const response = await fetch(`${API_BASE_URL}/welcome_videos/${pid}`, {
        method: "GET",
        headers: HEADERS,
      });
      const data = await response.json(); // Extracting data as a JSON Object from the response
      const needsWidget = data?.live;
      return { data, needsWidget };
    } catch (error) {
      console.log(error.message);
      return error.message;
    }
  };

  fetchAPIS().then(({ data = {}, needsWidget = true }) => {
    if (!needsWidget && prv) {
      window.alert("Show the preview bubble with the secret as company id");
    }
    if (needsWidget) {
      launchPreviewBubble(true);
    }
    function launchPreviewBubble(state) {
      const wrapper = document.createElement("div");
      const previewVideo = document.createElement("VIDEO");
      previewVideo.setAttribute("id", "myVideo");
      const source = document.createElement("SOURCE");
      source.setAttribute("src", data.question_1_video);
      source.setAttribute("type", "video/mp4");
      previewVideo.setAttribute(
        "poster",
        "https://www.joyoshare.com/images/resource/iphone-stuck-on-black-screen-with-spinning-wheel.jpg"
      );
      previewVideo.appendChild(source);
      previewVideo.autoplay = state;
      previewVideo.muted = true;
      previewVideo.loop = true;
      previewVideo.preload = "auto";

      previewVideo.style.cssText = `
                    height:135px;
                    width:250px;
                    object-fit:cover;
                    background-size: cover;
                    border-radius: 10px; 
                    border: 7px solid #eee;  
                    position:fixed;
                    bottom:42px;
                    right:40px;     
                    cursor:pointer;
                    opacity:1;
                    z-index: 99998 !important;


        `;

      const iconCancel = document.createElement("button");

      iconCancel.style.cssText = `
          height:25px;
          width:25px;
          background-color:rgba(0,0,0,.6);
          border-radius:50%;
          z-index:99999 !important;
          position:fixed;
          bottom:175px;
          right:40px; 
          color:#fff;
          padding:2px;
          cursor:pointer;
          border:none;
          outline:none;
          transition: all .3s ease-out;
         
       `;

      iconCancel.innerHTML = `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
        <path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z" />
        </svg>
        `;

      const smallDevice = window.matchMedia("(max-width: 700px)");
      if (smallDevice.matches) {
        // If media query matches
        previewVideo.style.cssText = `
          height:100px;
          width:100px;
          object-fit:cover;
          background-size: cover;
          border: 7px solid #eee;  
          position:fixed;
          bottom:23px;
          right:22px;
          border-radius:50%;
          z-index: 99998 !important;

          `;

        iconCancel.style.cssText = `
          height:24px;
          width:24px;
          background-color:rgba(0,0,0,.76);
          border-radius:50%;
          z-index:99999 !important;
          position:fixed;
          bottom:115px;
          right:20px; 
          color:#fff;
          padding:.12rem;
          border:none;
          outline:none;

         `;
      }
      if (!smallDevice.matches) {
        iconCancel.addEventListener("mouseenter", function () {
          iconCancel.style.cssText = `
                height:25px;
                width:25px;
                background-color:rgba(0,0,0,.6);
                border-radius:50%;
                z-index:99999 !important;
                position:fixed;
                bottom:175px;
                right:40px; 
                color:#fff;
                padding:2px;
                cursor:pointer;
                border:none;
                outline:none;
                transform: rotate(180deg);
                transition: all .3s ease-out;    
           `;
        });
        iconCancel.addEventListener("mouseleave", (event) => {
          iconCancel.style.cssText = `
            height:25px;
            width:25px;
            background-color:rgba(0,0,0,.6);
            border-radius:50%;
            z-index:99999 !important;
            position:fixed;
            bottom:175px;
            right:40px; 
            color:#fff;
            padding:2px;
            cursor:pointer;
            border:none;
            outline:none;    
            transition: all .3s ease-out
            `;
        });
        previewVideo.addEventListener("mouseenter", function () {
          previewVideo.style.cssText = `
            height:135px;
            width:250px;
            object-fit:cover;
            background-size: cover;
            border-radius: 10px; 
            border: 7px solid #eee;  
            position:fixed;
            bottom:42px;
            right:40px;     
            cursor:pointer;
            opacity:1;
            z-index: 99998 !important;
            scale:1.02;
            transition:all .3s linear;
        `;
        });

        previewVideo.addEventListener("mouseleave", function () {
          previewVideo.style.cssText = `
            height:135px;
            width:250px;
            object-fit:cover;
            background-size: cover;
            border-radius: 10px; 
            border: 7px solid #eee;  
            position:fixed;
            bottom:42px;
            right:40px;     
            cursor:pointer;
            opacity:1;
            z-index: 99998 !important;
            scale:1;
            transition:all .3s linear;
        `;
        });
      }

      iconCancel.addEventListener("click", function () {
        document.body.removeChild(wrapper);
      });
      previewVideo.addEventListener("click", function () {
        document.body.removeChild(wrapper);
        launchIFrame();
      });

      wrapper.appendChild(previewVideo);
      wrapper.appendChild(iconCancel);
      document.body.appendChild(wrapper);
    }

    function launchIFrame() {
      const iframeWrapper = document.createElement("div");
      const iframe = document.createElement("iframe");
      iframe.setAttribute("src", `${widgetUrl}/?pid=${pid}&prv=${prv}`);
      const iconFrameCancel = document.createElement("button");

      iconFrameCancel.style.cssText = `
        height:29px;
        width:29px;
        background-color:rgba(0,0,0,.6);
        top:38%;
        right:3.3%; 
        border-radius:50%;
        padding:.17rem;
        cursor:pointer;
        z-index:99999 !important;
        position:fixed;
        color:#fff;
        border:none;
        outline:none;
       `;

      iconFrameCancel.innerHTML = `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5">
        <path d="M6.28 5.22a.75.75 0 00-1.06 1.06L8.94 10l-3.72 3.72a.75.75 0 101.06 1.06L10 11.06l3.72 3.72a.75.75 0 101.06-1.06L11.06 10l3.72-3.72a.75.75 0 00-1.06-1.06L10 8.94 6.28 5.22z" />
        </svg>
        `;

      const smallDevice = window.matchMedia("(max-width: 700px)");
      const body = document.getElementsByTagName("body")[0];

      iframe.style.cssText = `
                    width:48vw;
                    height:53vh;
                    position:fixed;
                    bottom:3rem;
                    right:3rem;
                    border:none;
                    overflow:hidden;
                    border-radius:9px;
                    border:0;
                    overflow:hidden;    
                    background-color: rgba(0,0,0);

         `;

      if (smallDevice.matches) {
        // If media query matches
        body.style.cssText = `
        position:fixed;
        top:0;
        left:0;
        right:0;
        bottom:0;
        overflow:hidden !important;
           `;

        iframeWrapper.style.cssText = `
           height:100%;
           width:100%;
           background:rgba(0,0,0,.2);
           box-shadow: 0 8px 32px 0 rgba(255, 255, 255, 0.1);
           backdrop-filter: blur(3px);
           position:fixed;
           right:0;
           left:0;
           bottom:0;
           top:0;
           z-index:9999
           `;

        iframe.style.cssText = `
          width:100%;
          overflow:hidden;
          height:92%;
          position:fixed;
          bottom:0;
          right:0;
          border:0;
          border-top-right-radius:3px;
          border-bottom-left-radius:3px;

          `;
        iconFrameCancel.style.cssText = `
          transform: rotate(-180deg);
          height:23px;
          width:23px;
          background-color:rgba(0,0,0,.6);
          border-radius:50%;
          z-index:99999 !important;
          position:fixed;
          top:2.2%;
          right:4%; 
          color:#fff;
          padding:.17rem;
          cursor:pointer;
          border:none;
          outline:none;
          transition: all 0.1s linear;
          `;
        iconFrameCancel.addEventListener("click", function () {
          body.style.cssText = `      
              `;
          document.body.removeChild(wrapper);
          launchPreviewBubble(true);
        });
      }
      if (!smallDevice.matches) {
        iconFrameCancel.addEventListener("mouseenter", function () {
          iconFrameCancel.style.cssText = `
            height:29px;
            width:29px;
            background-color:rgba(0,0,0,.6);
            top:38%;
            right:3.3%; 
            border-radius:50%;
            padding:.17rem;
            cursor:pointer;
            z-index:99999 !important;
            position:fixed;
            color:#fff;
            border:none;
            outline:none;
            transform: rotate(90deg);
            transition: all .3s ease-out;  
           `;
        });

        iconFrameCancel.addEventListener("mouseleave", function () {
          iconFrameCancel.style.cssText = `
            height:29px;
            width:29px;
            background-color:rgba(0,0,0,.6);
            top:38%;
            right:3.3%; 
            border-radius:50%;
            padding:.17rem;
            cursor:pointer;
            z-index:99999 !important;
            position:fixed;
            color:#fff;
            border:none;
            outline:none;
            transform: rotate(-90deg);
            transition: all .3s ease-out;  
           `;
        });
      }

      iconFrameCancel.addEventListener("click", function () {
        document.body.removeChild(iframeWrapper);
        launchPreviewBubble(true);
        //wrapper.style.display = "none";
      });

      iframeWrapper.appendChild(iconFrameCancel);
      iframeWrapper.appendChild(iframe);
      document.body.appendChild(iframeWrapper);
    }
  });
});
