import Image from 'next/image'
/* eslint-disable import/no-cycle */
import styled, { ThemeContext } from 'styled-components'
import { useContext } from 'react'
import { BoldTextRegular, WrapperBox } from './index'
import { openInNewTab } from '../../../libs/helpers'

const Wrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.layout};
`

const WrapperBoxItem = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.lightestSlate};
  margin-bottom: ${({ theme }) => theme.gutters.medium};
  border-radius: 3px;
  display: grid;
  padding: 16px 19px 16px 19px;
  grid-template-columns: 2fr 6fr;
`

export const BoldText = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: 1.1rem;
`
const SchoolLogo = styled.div`
  height: 3.5rem;
  width: 3.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  overflow: hidden;
  position: relative;
  background: linear-gradient(
    0deg,
    rgba(242, 255, 246, 1) 0%,
    rgba(255, 241, 248, 1) 100%
  );

  margin-right: ${({ theme }) => theme.gutters.large};
`
const SchoolDesc = styled.div``
const TinyTxt = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  line-height: ${({ theme }) => theme.gutters.large};
  // margin-bottom: ${({ theme }) => theme.gutters.medium};
`
const Text = styled.p`
  color: ${({ theme }) => theme.colors.secondary};
  font-family: 'Caveat', cursive;
  font-size: ${({ theme }) => theme.gutters.regular};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  background: rgba(0, 0, 0, 0.3);
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  top: 0;
  left: 0;
`

function EducationBox({ item }) {
  return (
    <WrapperBoxItem>
      <SchoolLogo onClick={() => openInNewTab(item.school.websiteLink)}>
        <Image
          style={{ borderRadius: '50%', overflow: 'hidden' }}
          src={item.school.logoUrl}
          layout="fill"
          objectFit="contain"
        />
        <Text />
      </SchoolLogo>
      <SchoolDesc>
        <BoldText>{item.departmentName}</BoldText>
        <TinyTxt style={{ marginBottom: '6px' }}>{item?.school?.name},,,</TinyTxt>
        <TinyTxt>{item.courseName}</TinyTxt>
      </SchoolDesc>
    </WrapperBoxItem>
  )
}

function Education({ education }) {
  const { gutters } = useContext(ThemeContext)

  return (
    <WrapperBox>
      <BoldTextRegular
        style={{ marginBottom: gutters.small, marginTop: gutters.tiny }}
      >
        Education & Certifacations
      </BoldTextRegular>
      <Wrapper>
        {education.getEducation.map((item) => (
          <EducationBox key={item.id} item={item} />
        ))}
      </Wrapper>
    </WrapperBox>
  )
}

export default Education
