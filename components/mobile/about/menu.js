import { AnimatePresence, motion } from 'framer-motion'
// import { useRouter } from 'next/router'
import { useContext, useRef } from 'react'
import styled, { ThemeContext } from 'styled-components'

import { BsChevronRight } from 'react-icons/bs'
import useAuthStore from '../../../zustandStore/auth'

const Background = styled(motion.div)`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: flex-end;
  align-content: center;
  align-items: end;
  z-index: 9999;
`

const ImageClose = styled.div`
  cursor: pointer;
  font-family: 'Nunito-Bold';
  font-size: 16px;
  height: 55px;
  line-height: 16px;

  background-color: transparent;
  padding: 1.1rem;
  // border-bottom: 1px solid rgba(32, 110, 233);
  border-image-width: 1px;
  p {
    font-family: 'Nunito-Bold';
    font-style: normal;
    font-weight: 600;
  }
`

const ModalWrapper = styled.div`
  position: relative;
  background: ${({ theme }) => theme.colors.layout};
  // opacity: 0.99;
  width: 100vw;
  z-index: 99;
  left: 0;
  top: 0;
  height: 100%;
  overflow: hidden;
  text-align: center;
`

const MenuWrapper = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.small} 2px
    ${({ theme }) => theme.gutters.small};
`
const MenuDiv = styled.a`
  padding: 13px 7px;
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid ${({ theme }) => theme.colors.lightestSlate};
  text-decoration: none;
  color: ${({ theme }) => theme.colors.textSlate};
  align-items: center;
  /*  &:last-of-type {
    border-bottom: 1px solid transparent;
  } */
`
const MenuList = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  // font-weight: 600;
  //  padding: 8px 7px 7px;
  font-size: 15px;
`
const Button = styled.button`
  outline: none;
  border: none;
  padding: 11px 15px;
  background-color: ${({ theme }) => theme.colors.link};
  margin-bottom: 8px;
  border-radius: 3px;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: #e6f1ff;
  font-size: ${({ theme }) => theme.gutters.medium};
`
const OtherWrapper = styled.div`
  margin-top: 3rem;
  padding: 8px ${({ theme }) => theme.gutters.small} 2px
    ${({ theme }) => theme.gutters.small};
  align-self: flex-start;
  text-align: start;
`

const FooterWrapper = styled.div`
  position: absolute;
  bottom: 12px;
`

const CopyWrite = styled.p`
  font-family: 'Caveat', cursive;
  color: ${({ theme }) => theme.colors.textSlate};
  font-size: ${({ theme }) => theme.gutters.regular};
  line-height: ${({ theme }) => theme.gutters.xxlarge};
`
const Wrap = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-row-gap: 7px;
  justify-items: start;
`
const Auth = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-weight: 600;
  margin-top: 2rem;
`

function MenuModal({ showModal, setShowModal }) {
  const modalRef = useRef()
  const { colors, gutters } = useContext(ThemeContext)
  const { userProfile, removeUser } = useAuthStore()

  // const router = useRouter()

  /*  const onPush = (route) => {
    router.push(route)
  } */
  const onClick = (url) => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }
  return (
    <AnimatePresence exitBeforeEnte>
      {showModal && (
        <Background
          ref={modalRef}
          showModal={showModal}
          initial={{ opacity: 0, x: '-150%' }}
          animate={{ opacity: 1, x: '0' }}
          exit={{ opacity: 0, x: '150%' }}
          transition={{ duration: 0.3 }}
        >
          <ModalWrapper showModal={showModal}>
            <ImageClose />
            <MenuWrapper>
              <MenuDiv onClick={() => setShowModal(false)} href="#about">
                <MenuList>About</MenuList>
                <BsChevronRight color={colors.textSlate} fontSize={gutters.medium} />
              </MenuDiv>
              <MenuDiv onClick={() => setShowModal(false)} href="#work">
                <MenuList>Work</MenuList>
                <BsChevronRight color={colors.textSlate} fontSize={gutters.medium} />
              </MenuDiv>
              <MenuDiv onClick={() => setShowModal(false)} href="#projects">
                <MenuList>Projects</MenuList>
                <BsChevronRight color={colors.textSlate} fontSize={gutters.medium} />
              </MenuDiv>
              <MenuDiv onClick={() => setShowModal(false)} href="#contact">
                <MenuList>Contact</MenuList>
                <BsChevronRight color={colors.textSlate} fontSize={gutters.medium} />
              </MenuDiv>
            </MenuWrapper>
            <OtherWrapper>
              <Button
                style={{ marginRight: gutters.large }}
                onClick={() => onClick('/assets/pdf/cv.pdf')}
              >
                Download Resume
              </Button>
              {userProfile && (
                <Auth
                  onClick={() => {
                    removeUser()
                  }}
                >
                  Log out
                </Auth>
              )}
              <FooterWrapper>
                <Wrap style={{ paddingTop: '1rem' }}>
                  <CopyWrite>&#169; mwangi {new Date().getFullYear()}</CopyWrite>
                </Wrap>
              </FooterWrapper>
            </OtherWrapper>
          </ModalWrapper>
        </Background>
      )}
    </AnimatePresence>
  )
}

export default MenuModal
