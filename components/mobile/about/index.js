import Head from 'next/head'
/* eslint-disable import/no-cycle */
import styled, { ThemeContext } from 'styled-components'
import { useContext, useEffect } from 'react'
// import dynamic from 'next/dynamic'
// import { Suspense } from 'react'
import TechStack from './techStack'
import Intro from './introduction'
import CareerJourney from './careerJourney'
// import CurrentWorkplace from './CurrentWorkplace'

// import SocialHandles from './socialHandles'

// import Education from './education'

import WorkHistory from './workHistory'
import { useGlobalContext } from '../../../context/provider'
import Footer from './footer'
import Projects from './projects'
import { CLOSE_LOADING, OPEN_LOADING } from '../../../constants'

const Wrapper = styled.div`
  position: relative;
  // filter: ${({ bg }) => (bg === 'blur' ? 'grayscale(0.5) blur(10px)' : '')};
  // overflow: hidden;
  // height: 100vh;
  background-color: ${({ theme }) => theme.colors.background};
  -webkit-overflow-scrolling: touch;
  // filter: grayscale(0.5) blur(10px);
`

const MobileEntryWrapper = styled.div`
  margin-top: 55px;
  width: 100%;
  height: 100%;
  overflow: auto;
`

export const WrapperBox = styled.div`
  background-color: ${({ theme }) => theme.colors.layout};
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
  margin-top: ${({ theme }) => theme.gutters.xsmall};
  &:last-of-type {
    margin-bottom: 15px;
  }
`

export const NormalText = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
  font-size: 15px;
  line-height: calc(26 / 16);
`
export const NormalTextExp = styled.span`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
`
export const BoldText = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: ${({ theme }) => theme.gutters.medium};
  line-height: ${({ theme }) => theme.gutters.xlarge};
`
export const BoldTextRegular = styled(BoldText)`
  font-size: 19px;
  line-height: calc(28 / 25);
  font-family: Larsseit, Inter var, -apple-system, BlinkMacSystemFont, Segoe UI,
    Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
    sans-serif;
  font-weight: 700;
  scroll-margin-top: 2.5rem;
  color: ${({ theme }) => theme.colors.white};
`
export const BoldTextHuge = styled(BoldText)`
  font-size: ${({ theme }) => theme.gutters.xlarge};
`

const index = ({ themeToggler, theme }) => {
  // const [loading, setLoading] = useState(false)

  /*  useEffect(() => {
    setLoading(true)
    setTimeout(() => setLoading(false), 700)
  }) */

  const { loadingDispatch } = useGlobalContext()

  useEffect(() => {
    loadingDispatch({ type: OPEN_LOADING })
    setTimeout(() => {
      loadingDispatch({ type: CLOSE_LOADING })
    }, 2600)
  }, [])

  const { colors } = useContext(ThemeContext)

  const { menuState } = useGlobalContext()

  /*   if (loading) {
    return <InLoader />
  } */

  return (
    <Wrapper bg={menuState?.open ? 'blur' : 'normal'}>
      <Head>
        <meta
          name="theme-color"
          media="(prefers-color-scheme: light)"
          content={colors.background}
        />
      </Head>
      <MobileEntryWrapper>
        <Intro />
        <CareerJourney />
        <TechStack />
        {/*    <CurrentWorkplace homeVideo={homeVideo} /> */}
        <WorkHistory />
        <Projects />
        {/*   <Education education={education} /> */}
        {/*  <SocialHandles socials={socials} /> */}
        {/*    <CopyWrite /> */}
        <Footer theme={theme} themeToggler={themeToggler} />
      </MobileEntryWrapper>
    </Wrapper>
  )
}

export default index
