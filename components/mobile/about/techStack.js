/* eslint-disable import/no-cycle */
import { useContext } from 'react'
import { ThemeContext } from 'styled-components'
import { WrapperBox, BoldTextRegular } from './index'
import Accordion from './accordion'

const techStack = () => {
  const { gutters } = useContext(ThemeContext)
  return (
    <WrapperBox
      initial={{ opacity: 0 }}
      transition={{ delay: 0.1 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
    >
      <BoldTextRegular
        style={{ marginBottom: gutters.small, marginTop: gutters.tiny }}
      >
        My focus tech stack
      </BoldTextRegular>
      <Accordion />
    </WrapperBox>
  )
}

export default techStack
