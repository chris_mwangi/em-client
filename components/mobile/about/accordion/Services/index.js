import styled from 'styled-components'
import ImageContainer from '../../../ImageHolder'

const Wrap = styled.div`
  display: flex;
  align-items: center;
`
const TechList = styled.p`
  font-size: ${({ theme }) => theme.gutters.medium};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  color: ${({ theme }) => theme.colors.textSlate};
`

function Services() {
  return (
    <>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Graphcms</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Production and the cloud(Docker+Aws)</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Sanity io</TechList>
      </Wrap>

      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Aws services</TechList>
      </Wrap>
    </>
  )
}

export default Services
