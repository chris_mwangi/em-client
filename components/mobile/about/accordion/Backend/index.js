import React from 'react'
import styled from 'styled-components'
import ImageContainer from '../../../ImageHolder'

const Wrap = styled.div`
  display: flex;
  align-items: center;
`
const TechList = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  color: ${({ theme }) => theme.colors.textSlate};
  font-size: ${({ theme }) => theme.gutters.medium};
`

function Backend() {
  return (
    <>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Nodejs, Express Rest apis</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Nodejs,Appolo-express-server Graphql apis</TechList>
      </Wrap>

      {/*  <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Sockets with nodejs</TechList>
      </Wrap> */}

      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Firebase</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Aws amplify</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Mongodb</TechList>
      </Wrap>
    </>
  )
}

export default Backend
