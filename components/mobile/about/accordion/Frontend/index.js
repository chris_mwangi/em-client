import React from 'react'
import styled from 'styled-components'
import ImageContainer from '../../../ImageHolder'

const Wrap = styled.div`
  display: flex;
  align-items: center;
`
const TechList = styled.p`
  font-size: ${({ theme }) => theme.gutters.medium};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  color: ${({ theme }) => theme.colors.textSlate};
`

function Frontend() {
  return (
    <>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Javascript/Typescript</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>React/Nextjs/Gatsbayjs(Isr,Ssr,Csr,Ssg)</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>React Native</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Redux/redux toolkit (rest apis)</TechList>
      </Wrap>

      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Css/Sass/Styled components/tailwind</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Jest/supertest (TDD using unit test)</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Vercel/Aws-CI/CD provider with gitlab</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Appolo client(Graphql apis)</TechList>
      </Wrap>
    </>
  )
}

export default Frontend
