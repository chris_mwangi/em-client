import styled from 'styled-components'
import ImageContainer from '../../../ImageHolder'

const Wrap = styled.div`
  display: flex;
  align-items: center;
`
const TechList = styled.p`
  font-size: ${({ theme }) => theme.gutters.medium};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  color: ${({ theme }) => theme.colors.textSlate};
`

function Project() {
  return (
    <>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Shortcut (previous ClubHouse) for tasks</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Slack (communication with team)</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Trello</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Loom (Video service for tasks)</TechList>
      </Wrap>
      <Wrap>
        <ImageContainer
          src="/assets/mobile/images/tick.svg"
          height="23px"
          width="23px"
        />
        <TechList>Gitlab</TechList>
      </Wrap>
    </>
  )
}

export default Project
