import { memo, useState } from 'react'
import styled from 'styled-components'
import Frontend from './Frontend'
import Backend from './Backend'
import Services from './Services'
import Projectmanagement from './Projectmanagement'

const AccordionWrapper = styled.div`
  background: transparent;
`
const ContentWrapper = styled.div`
  border-top: 1px solid ${(props) => props.theme.colors.graySlight};
  &:last-child {
    border-bottom: 1px solid ${(props) => props.theme.colors.graySlight};
  }
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: 17px;
`
const TextIconWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: ${({ theme }) => theme.gutters.small} 0;
`
const Text = styled.p`
  font-size: ${({ theme }) => theme.gutters.regular};
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.textSlate};
`
const Icon = styled.svg`
  color: ${({ theme }) => theme.colors.text};
  height: 23px;
  width: 23px;
`

const InternalWrapper = styled.div`
  width: 100%;
  height: ${(props) => (props.open ? '100%' : '0')};
  transition: all 0.5s linear;
  overflow: auto;
`
const Wrap = styled.div`
  transition: all 0.5s linear;
`

const index = () => {
  const [showMoreFrontend, setshowMoreFrontend] = useState(false)
  const [showMoreBackend, setshowMoreBackend] = useState(false)
  const [showMoreServices, setshowMoreServices] = useState(false)
  const [showMoreManagement, setshowMoreManagement] = useState(false)

  return (
    <AccordionWrapper>
      <ContentWrapper>
        <TextIconWrapper
          onClick={() => {
            setshowMoreFrontend(!showMoreFrontend)
          }}
        >
          <Text>Frontend </Text>
          {!showMoreFrontend ? (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="dark:text-slate-300"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M12 4v16m8-8H4"
              />
            </Icon>
          ) : (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M20 12H4"
              />
            </Icon>
          )}
        </TextIconWrapper>
        {showMoreFrontend && (
          <InternalWrapper open={showMoreFrontend}>
            <Wrap>
              <Frontend />
            </Wrap>
          </InternalWrapper>
        )}
      </ContentWrapper>
      <ContentWrapper>
        <TextIconWrapper
          onClick={() => {
            setshowMoreBackend(!showMoreBackend)
          }}
        >
          <Text>Backend</Text>
          {!showMoreBackend ? (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M12 4v16m8-8H4"
              />
            </Icon>
          ) : (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M20 12H4"
              />
            </Icon>
          )}
        </TextIconWrapper>
        {showMoreBackend && (
          <InternalWrapper open={showMoreBackend}>
            <Wrap>
              <Backend />
            </Wrap>
          </InternalWrapper>
        )}
      </ContentWrapper>
      <ContentWrapper>
        <TextIconWrapper
          onClick={() => {
            setshowMoreServices(!showMoreServices)
          }}
        >
          <Text>Services/Headless cms etc</Text>
          {!showMoreServices ? (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M12 4v16m8-8H4"
              />
            </Icon>
          ) : (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M20 12H4"
              />
            </Icon>
          )}
        </TextIconWrapper>
        {showMoreServices && (
          <InternalWrapper open={showMoreServices}>
            <Wrap>
              <Services />
            </Wrap>
          </InternalWrapper>
        )}
      </ContentWrapper>
      <ContentWrapper>
        <TextIconWrapper
          onClick={() => {
            setshowMoreManagement(!showMoreManagement)
          }}
        >
          <Text>Project Management</Text>
          {!showMoreManagement ? (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M12 4v16m8-8H4"
              />
            </Icon>
          ) : (
            <Icon
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M20 12H4"
              />
            </Icon>
          )}
        </TextIconWrapper>
        {showMoreManagement && (
          <InternalWrapper open={showMoreManagement}>
            <Wrap>
              <Projectmanagement />
            </Wrap>
          </InternalWrapper>
        )}
      </ContentWrapper>
    </AccordionWrapper>
  )
}

export default memo(index)
