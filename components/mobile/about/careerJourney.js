/* eslint-disable import/no-cycle */
import { useContext } from 'react'

import styled, { ThemeContext } from 'styled-components'
// import { BsFillEmojiSmileFill } from 'react-icons/bs'
import {
  WrapperBox,
  NormalText,
  // NormalText,
  BoldTextRegular,
} from './index'

// import { openInNewTab } from '../../../libs/helpers'

const Description = styled.div`
  margin-bottom: 12px;
`

const career = () => {
  const { gutters } = useContext(ThemeContext)

  return (
    <WrapperBox
      initial={{ opacity: 0 }}
      transition={{ delay: 0.1 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
    >
      {/*    <div
        style={{ background: '#142a4a', paddingTop: '14px', paddingBottom: '14px' }}
      >
        {' '} */}
      <BoldTextRegular
        style={{
          marginBottom: gutters.small,
          marginTop: gutters.tiny,
        }}
      >
        Me in nutshell
      </BoldTextRegular>
      <Description>
        <NormalText style={{ marginBottom: gutters.tiny }}>
          I enjoy creating things that live on the internet. My interest in web
          development kickstarted back in 2017 when I decided to try editing
          templates and the feeling was wow!.
        </NormalText>
      </Description>

      {/*  <Description>
        <NormalTextExp style={{ marginBottom: gutters.tiny }}>
          After some time,i started exploring logic side of things.Initially i had
          loved Javascript so backend exploration became handy with its
          runtime(nodejs).
        </NormalTextExp>
      </Description>
 */}
      <Description>
        <NormalText style={{ marginBottom: gutters.tiny }}>
          Fast-forward to today,I’ve had the privilege of working at a creator
          economy company, a start-up, and an advertising agency.
        </NormalText>
      </Description>
    </WrapperBox>
  )
}

export default career
