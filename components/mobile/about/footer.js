import { motion } from 'framer-motion'
import styled, { ThemeContext } from 'styled-components'
import { AiFillGitlab, AiFillLinkedin, AiFillMail } from 'react-icons/ai'
import { useContext } from 'react'
import { BsWhatsapp } from 'react-icons/bs'
import { openInNewTab } from '../../../libs/helpers'

const Wrapper = styled(motion.div)`
  background-color: ${({ theme }) => theme.colors.layout};
  padding: 8px ${({ theme }) => theme.gutters.medium} 11px
    ${({ theme }) => theme.gutters.medium};
`

const CopyWrite = styled.p`
  font-family: 'Caveat', cursive;
  color: ${({ theme }) => theme.colors.textSlate};
  font-size: ${({ theme }) => theme.gutters.regular};
  line-height: ${({ theme }) => theme.gutters.xxlarge};
  text-align: center;
`
const Wrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 18px;
`

const SocialWrap = styled.div`
  display: flex;
  align-items: center;
`
const IconWrap = styled.span`
  margin-right: 6px;
  padding: 3px 7px;
`
const NormalText = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
  font-size: 15px;
  line-height: calc(24 / 16);
  margin: 0 !important;
`
const D = styled.div`
  margin-bottom: 6px;
  .n {
    font-size: 16px;
    font-weight: 500;
  }
`
const BoldText = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: 1.07rem;
  margin-bottom: 4px;
`
function ConnectBox({ social }) {
  if (social.name === 'gmail') {
    return (
      <SocialWrap>
        <a style={{ textDecoration: 'none' }} href="mailto:emmamwas99@gmail.com">
          <IconWrap>{social.icon}</IconWrap>
        </a>
      </SocialWrap>
    )
  }
  return (
    <SocialWrap>
      <IconWrap onClick={() => openInNewTab(social?.link)}>{social.icon}</IconWrap>
    </SocialWrap>
  )
}

function Footer() {
  const { colors, gutters } = useContext(ThemeContext)
  const socials = [
    {
      id: 1,
      name: 'gitlab',
      icon: <AiFillGitlab color={colors.textSlate} fontSize={gutters.xlarge} />,
      link: 'https://gitlab.com/chris_mwangi',
    },
    {
      id: 2,
      name: 'whatsap',
      icon: <BsWhatsapp color={colors.textSlate} fontSize={gutters.regular} />,
      link: 'https://wa.me/+254791608150',
    },
    {
      id: 3,
      name: 'Linkedin',
      icon: <AiFillLinkedin color={colors.textSlate} fontSize={gutters.xlarge} />,
      link: 'https://www.linkedin.com/in/mwangi-maina-6463281ab/',
    },
    {
      id: 4,
      name: 'Gmail',
      icon: <AiFillMail color={colors.textSlate} fontSize={gutters.xlarge} />,
      link: 'mailto:emmamwas99@gmail.com',
    },
  ]
  return (
    <Wrapper
      id="contact"
      initial={{ opacity: 0 }}
      transition={{ delay: 0.3 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
    >
      <div style={{ display: 'none' }}>
        <BoldText>Referees</BoldText>
        <D>
          <NormalText className="n">Kannan Rhegu (CTO,Playback)</NormalText>
          <NormalText>kannan@letsplayback.com/+491734128500</NormalText>
        </D>
        <D>
          <NormalText className="n">
            Ajesh K Gopi(UI/UX designer,Playback)
          </NormalText>
          <NormalText>ajeshkgopi9566@gmail.com/+918111920413</NormalText>
        </D>
        <D>
          <NormalText className="n">
            Caxton Muthoni (Fullstack Engineer Atilead)
          </NormalText>
          <NormalText>githinjicaxton323@gmail.com/+254743751575</NormalText>
        </D>
      </div>
      <Wrap>
        {socials?.map((social) => (
          <ConnectBox key={social?.id} social={social} />
        ))}
      </Wrap>

      <CopyWrite>&#169; mwangi {new Date().getFullYear()}</CopyWrite>
    </Wrapper>
  )
}

export default Footer
