/* eslint-disable import/no-cycle */
// import { useRouter } from 'next/router'
import { motion } from 'framer-motion'
import { useContext, useRef } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { BoldText, NormalText } from './index'

/* const Button = styled.button`
  outline: none;
  border: none;
  padding: 9px 14px;
  background-color: ${({ theme }) => theme.colors.link};
  margin-bottom: 8px;
  border-radius: 7px;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: #e6f1ff;
  font-size: ${({ theme }) => theme.gutters.medium};
` */
const WrapperBox = styled(motion.div)`
  background-color: ${({ theme }) => theme.colors.layout};
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
  margin-top: ${({ theme }) => theme.gutters.xsmall};
  // border: 1px solid ${({ theme }) => theme.colors.lightestSlate};
  border-radius: 1px;
`

const Do = styled.p`
  font-weight: 900;
  font-size: 2.1rem;
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin-bottom: 12px;
  color: ${({ theme }) => theme.colors.white};
  margin-top: 12px;
  color: #8892b0;
  font-family: 'Nunito-Black';
`
const introduction = () => {
  const { gutters, colors } = useContext(ThemeContext)
  const scrollRef = useRef(null)

  /*  const router = useRouter() */

  /*   const onClick = (url) => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  } */
  /*   const onMove = (url) => {
    router.push(url)
  } */
  return (
    <WrapperBox
      id="about"
      ref={scrollRef}
      initial={{ opacity: 0, scale: 1, y: '2%' }}
      transition={{ delay: 0.3, y: 0, scale: 1.02 }}
      whileInView={{ opacity: 1, y: 0, scale: 1 }}
      viewport={{ once: true }}
    >
      <div>
        <NormalText style={{ marginBottom: '12px', fontSize: '19px' }}>
          Hi there.My name is
        </NormalText>
        <BoldText
          style={{
            marginBottom: gutters.small,
            color: colors.white,
            fontSize: '25px',
            margin: '17px 0',
            // lineHeight: '27px',
          }}
        >
          Mwangi E.Maina.
        </BoldText>
        <Do>I build websites and mobile apps.</Do>
      </div>
      {/*  <div style={{ marginBottom: gutters.tiny }}>
        <BoldTextHuge
          style={{ marginBottom: '4px', fontWeight: 800, color: colors.text }}
        >
          MERNG developer{' '}
        </BoldTextHuge>
      </div> */}
      {/*  <NormalText style={{ marginBottom: gutters.tiny }}>
        Frontend engineer (Web/Mobile) at Playback and the thinker you need.
      </NormalText> */}
      <NormalText style={{ marginBottom: gutters.small }}>
        Forward looking and detail-oriented software engineer with 4+ years
        background in creating and executing innovative software solutions to enhance
        business productivity.Recently, i have been focusing on building accessible,
        creator-centered products at{' '}
        <a
          style={{ color: colors.textFooter }}
          href="https://letsplayback.com"
          target="_blank"
          rel="noreferrer"
        >
          Playback,
        </a>{' '}
      </NormalText>
      {/*    <Button
        style={{ marginRight: gutters.large }}
        onClick={() => onClick('/assets/pdf/cv.pdf')}
      >
        Download Resume
      </Button> */}
      {/*   <Button onClick={() => onMove('chat')}>Let&apos;s talk</Button> */}
    </WrapperBox>
  )
}

export default introduction
