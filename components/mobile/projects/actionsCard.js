import styled, { ThemeContext } from 'styled-components'
import { BsHeartFill, BsChat, BsSuitHeart } from 'react-icons/bs'
import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { useMutation } from '@apollo/client'
import { LIKE_PROJECT } from '../../../apollo/mutations/likeProject'
import { useGlobalContext } from '../../../context/provider'
import { CLOSE_LOADING, OPEN_ERROR, OPEN_LOADING } from '../../../constants'
import useAuthStore from '../../../zustandStore/auth'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  row-gap: 10px;
  z-index: 100;
  padding: ${({ theme }) => theme.gutters.large};
`

function ActionCard({ project }) {
  const { colors, gutters } = useContext(ThemeContext)
  const [liked, setLiked] = useState(false)

  const [handleLikeProject] = useMutation(LIKE_PROJECT, { useCdn: false })

  const { loadingDispatch, errorDispatch } = useGlobalContext()
  const { userProfile } = useAuthStore()

  const router = useRouter()

  const navigate = (url, name, action) => {
    router.push({ pathname: url, query: { name, action } })
  }

  useEffect(() => {
    const userContainedInLikes =
      userProfile?.displayName &&
      project?.projectlikes?.find(
        (like) => like?.userName === userProfile.displayName
      )

    if (userContainedInLikes) {
      setLiked(true)
    } else {
      setLiked(false)
    }
  }, [project?.projectlikes, userProfile])

  const onLike = async () => {
    loadingDispatch({
      type: OPEN_LOADING,
    })
    try {
      const {
        data: {
          likeProject: { errors },
        },
      } = await handleLikeProject({
        variables: {
          projectId: project.id,
        },
      })

      if (errors) {
        const errorMessage = errors?.message
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }
      loadingDispatch({
        type: CLOSE_LOADING,
      })
    } catch (error) {
      errorDispatch({
        type: OPEN_ERROR,
        payload: 'Server Error!',
      })
      loadingDispatch({
        type: CLOSE_LOADING,
      })
    }
  }

  return (
    <Wrapper>
      {liked ? (
        <BsHeartFill
          onClick={() => onLike()}
          color={colors.primary}
          fontSize={gutters.large}
        />
      ) : (
        <BsSuitHeart
          onClick={() => onLike()}
          color={colors.tertiary}
          fontSize={gutters.large}
        />
      )}

      <BsChat
        onClick={() =>
          navigate(`projects/${project.id}`, project.projectTitle, 'comments')
        }
        color={colors.text}
        fontSize={gutters.large}
      />
    </Wrapper>
  )
}

export default ActionCard
