import { useQuery } from '@apollo/client'
import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import styled from 'styled-components'
import { GET_PROJECTS } from '../../../apollo/queries/projects'
import { WrapperBox } from '../about'
import Loader from './loader'
// import Filters from './filters'

const CompanyCard = dynamic(() => import('./companyCard'), {
  suspense: true,
})

const ProjectCard = dynamic(() => import('./projectCard'), {
  suspense: true,
})

const LikesCommentsCard = dynamic(() => import('./likesCommentsCard'), {
  suspense: true,
})

const MobileEntryWrapper = styled.div`
  margin-top: 63px;
  width: 100%;

  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`

const index = () => {
  const { data, loading } = useQuery(GET_PROJECTS, { ssr: false })
  const projects = data?.getProjects.projects
  if (loading) {
    return <Loader />
  }
  return (
    <MobileEntryWrapper>
      {projects?.map((project) => (
        <Suspense fallback="Loading items">
          <WrapperBox key={project.id} style={{ borderRadius: '3px' }}>
            <CompanyCard project={project} />
            <ProjectCard project={project} />
            <LikesCommentsCard project={project} />
          </WrapperBox>
        </Suspense>
      ))}
    </MobileEntryWrapper>
  )
}

export default index
