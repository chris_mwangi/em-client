import Image from 'next/image'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import ActionCard from './actionsCard'

const Wrapper = styled.div`
  height: 14rem;
  border-radius: 8px;
  margin-top: 7px;
  position: relative;
`
const ImageWrapper = styled.div`
  height: 100%;
  width: 100%;
  border-radius: 5px;
  border: 1px solid #eee;
`
const Img = styled(Image)`
  border-radius: 5px;
`

const CTA = styled.div`
  position: absolute;
  right: 11px;
  bottom: 10px;
`
const Name = styled.span`
  background: rgba(69, 174, 202, 0.06);
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 3px;
  display: flex;
  align-items: center;
  justify-content: center;
`

function ProjectCard({ project }) {
  const router = useRouter()
  const navigate = (url, name) => {
    router.push({ pathname: url, query: { name } })
  }
  return (
    <Wrapper>
      <ImageWrapper
        onClick={() => navigate(`projects/${project.id}`, project.projectTitle)}
      >
        <Img
          src={project?.projectImages[0]}
          objectFit="cover"
          quality={100}
          alt=".."
          layout="fill"
        />
        <Name />
      </ImageWrapper>
      <CTA>
        <ActionCard project={project} />
      </CTA>
    </Wrapper>
  )
}

export default ProjectCard
