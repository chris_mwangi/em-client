/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import { useMutation } from '@apollo/client'
import Image from 'next/image'
import { memo, useContext, useEffect, useState } from 'react'
import { BsFillChatTextFill, BsFillHandThumbsUpFill } from 'react-icons/bs'

import styled, { ThemeContext } from 'styled-components'
import {
  LIKE_COMMENT,
  LIKE_COMMENT_REPLY,
} from '../../../../apollo/mutations/likeComments'
import { CLOSE_LOADING, OPEN_ERROR, OPEN_LOADING } from '../../../../constants'
import { useGlobalContext } from '../../../../context/provider'
import useAuthStore from '../../../../zustandStore/auth'
// import useAuthStore from '../../../../store/authStore'

const Wrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.colors.chatBg};

  margin: 6px;
  max-width: 67vw;
  min-width: 46vw;
  padding: 12px 16px;
  border-radius: 0 8px 8px 8px;
`
const ProfilePic = styled.div`
  background-color: #eeee;
  height: 32px;
  width: 32px;
  position: relative;
  border-radius: 50%;
  margin-top: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
`

const Body = styled.div`
  flex-basis: 82%;
  font-size: 15px;
  color: #020202;
  display: flex;
  flex-direction: column;
  font-family: ${({ theme }) => theme.fonts.Apple};
`
const Username = styled.span`
  font-size: 13px;
  color: ${({ theme }) => theme.colors.text};
  margin-bottom: 0;
  font-weight: 600;
  font-family: ${({ theme }) => theme.fonts.Apple};
`
const Text = styled.span`
  font-size: 15px;
  color: ${({ theme }) => theme.colors.text};
  line-height: 23px;
  font-family: ${({ theme }) => theme.fonts.Apple};
`

const Thumb = styled.svg`
  height: 17px;
  width: 17px;
  margin-left: 3px;
  color: ${(props) => props.liked && '#1068bf'};
  // transform: rotate(90deg);
`

const Replyheader = styled.div`
  display: ${(props) => !props.not && 'flex'};
  align-items: center;
  margin-left: 12%;
  margin-top: 12px;
  margin-bottom: 12px;
  max-width: 67vw;
  min-width: 46vw;
  span {
    font-size: 13px;
    margin-right: 0.4rem;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`

const OneReply = styled.div``

const commentbox = ({ comment, id, setReplyCommentObj }) => {
  const [liked, setLiked] = useState(false)
  const { colors } = useContext(ThemeContext)
  const [viewreplies, setViewreplies] = useState(false)
  const { errorDispatch, loadingDispatch } = useGlobalContext()
  const { userProfile } = useAuthStore()

  useEffect(() => {
    const userContainedInLikes =
      userProfile?.displayName &&
      comment?.likes?.find((like) => like?.userName === userProfile?.displayName)

    if (userContainedInLikes) {
      setLiked(true)
    } else {
      setLiked(false)
    }
  }, [userProfile, comment])

  const [likeComment] = useMutation(LIKE_COMMENT, {})

  const [likeCommentReply] = useMutation(LIKE_COMMENT_REPLY, {})

  const onSubmitLikeComment = async () => {
    try {
      loadingDispatch({
        type: OPEN_LOADING,
      })
      const {
        data: {
          likeComment: { projects, errors },
        },
      } = await likeComment({
        variables: {
          projectId: id,
          commentId: comment?.id,
        },
      })
      if (errors) {
        const errorMessage = errors?.message
        loadingDispatch({
          type: CLOSE_LOADING,
        })
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }

      if (projects) {
        loadingDispatch({
          type: CLOSE_LOADING,
        })
      }
    } catch (error) {
      return loadingDispatch({
        type: CLOSE_LOADING,
      })
    }
    return {}
  }

  const onSubmitLikeCommentReply = async (replyId) => {
    try {
      loadingDispatch({
        type: OPEN_LOADING,
      })
      const {
        data: {
          likeCommentReply: { projects, errors },
        },
      } = await likeCommentReply({
        variables: {
          projectId: id,
          commentId: comment?.id,
          replyId,
        },
      })
      if (errors) {
        const errorMessage = errors?.message
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }

      if (projects) {
        loadingDispatch({
          type: CLOSE_LOADING,
        })
      }
    } catch (error) {
      return loadingDispatch({
        type: CLOSE_LOADING,
      })
    }
    return {}
  }

  return (
    <>
      <div style={{ display: 'flex', alignItems: 'flex-start', marginLeft: '12px' }}>
        <ProfilePic>
          <Image
            layout="fill"
            src={comment?.profilePicUrl}
            objectFit="cover"
            style={{ borderRadius: '50%' }}
          />
        </ProfilePic>
        <Wrapper>
          <Body>
            <Username>{comment?.userName || 'Author'}</Username>
            <Text>{comment?.body}</Text>
            {comment?.assetUrl !== '' && (
              <Image
                src={comment?.assetUrl}
                objectFit="contain"
                height={100}
                style={{ marginTop: '3px' }}
                width="100%"
              />
            )}
          </Body>
        </Wrapper>
      </div>
      <div>
        <Replyheader>
          <span
            style={{ display: 'flex', color: colors.text, alignItems: 'center' }}
          >
            Like
            {liked ? (
              <div
                onClick={() => onSubmitLikeComment()}
                style={{ padding: '5px', display: 'flex', alignItems: 'center' }}
              >
                <BsFillHandThumbsUpFill color={colors.primary} />
              </div>
            ) : (
              <div
                onClick={() => onSubmitLikeComment()}
                style={{ padding: '5px', display: 'flex', alignItems: 'center' }}
              >
                <BsFillHandThumbsUpFill color={colors.text} />
              </div>
            )}{' '}
          </span>
          <span style={{ color: colors.text }}>|</span>
          <span
            style={{
              color: colors.text,
              display: 'flex',
              alignItems: 'center',
              columnGap: '3px',
            }}
            onClick={() =>
              setReplyCommentObj({ id: comment?.id, userName: comment?.userName })
            }
          >
            <BsFillChatTextFill color={colors.text} />
            Reply
          </span>
          <span>.</span>

          <span style={{ color: colors.text }}>{comment?.likes?.length} likes</span>
        </Replyheader>
        <Replyheader not>
          {comment?.replies?.length > 1 && !viewreplies && (
            <span
              onClick={() => setViewreplies(true)}
              style={{ color: colors.text, fontWeight: 600 }}
            >
              Show previous replies...
            </span>
          )}
          {!viewreplies && comment?.replies?.length > 0 && (
            <OneReply style={{ marginTop: '7px' }}>
              <div
                style={{
                  display: 'flex',
                  alignItems: 'flex-start',
                  marginLeft: '12px',
                }}
              >
                <ProfilePic>
                  <img
                    src={comment.replies[0]?.profilePicUrl}
                    objectFit="contain"
                    style={{ borderRadius: '50%', height: '24px', width: '24px' }}
                    alt="..."
                  />
                </ProfilePic>
                <Wrapper>
                  <Body>
                    <Username>{comment?.replies[0]?.userName || 'Author'}</Username>
                    <Text>{comment?.replies[0]?.body}</Text>
                    {comment?.replies[0]?.assetUrl !== '' && (
                      <Image
                        src={comment?.replies[0]?.assetUrl}
                        objectFit="contain"
                        height={100}
                        style={{ marginTop: '3px' }}
                        width="100%"
                      />
                    )}
                  </Body>
                </Wrapper>
              </div>
              <span
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  marginLeft: '3.1rem',
                  color: colors.text,
                }}
              >
                Like
                {comment.replies[0]?.likes.find(
                  (like) => like?.userName === userProfile?.displayName
                ) ? (
                  <div
                    onClick={() => onSubmitLikeCommentReply(comment?.replies[0]?.id)}
                    style={{
                      padding: '5px',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Thumb liked>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-5 w-5"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                      >
                        <path d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                      </svg>
                    </Thumb>
                  </div>
                ) : (
                  <div
                    onClick={() => onSubmitLikeCommentReply(comment?.replies[0]?.id)}
                    style={{
                      padding: '5px',
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <Thumb>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        strokeWidth={2}
                        onClick={() =>
                          onSubmitLikeCommentReply(comment?.replies[0]?.id)
                        }
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5"
                        />
                      </svg>
                    </Thumb>
                  </div>
                )}{' '}
                <span style={{ color: colors.text }}>|</span>
                <span>.</span>
                {comment?.replies?.length > 0 && (
                  <span style={{ color: colors.text }}>
                    {comment?.replies[0].likes?.length} likes
                  </span>
                )}
              </span>
            </OneReply>
          )}

          {viewreplies &&
            comment?.replies?.length > 1 &&
            comment?.replies?.map((reply) => (
              <OneReply style={{ marginTop: '7px' }}>
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'flex-start',
                    marginLeft: '12px',
                  }}
                >
                  {console.log(reply?.profilePicUrl)}
                  <ProfilePic>
                    <img
                      alt="...."
                      layout="fill"
                      src={reply?.profilePicUrl}
                      objectFit="contain"
                      style={{ borderRadius: '50%', height: '30px', width: '30px' }}
                    />
                  </ProfilePic>
                  <Wrapper>
                    <Body>
                      <Username>{reply?.userName || 'Author'}</Username>
                      <Text>{reply?.body}</Text>
                      {reply?.assetUrl !== '' && (
                        <Image
                          src={reply?.assetUrl}
                          objectFit="contain"
                          height={100}
                          style={{ marginTop: '3px' }}
                          width="100%"
                        />
                      )}
                    </Body>
                  </Wrapper>
                </div>
                <span
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    marginLeft: '3.1rem',
                    color: colors.text,
                  }}
                >
                  Like
                  {reply?.likes.find(
                    (like) => like?.userName === userProfile?.displayName
                  ) ? (
                    <div
                      onClick={() => onSubmitLikeCommentReply(reply?.id)}
                      style={{
                        padding: '5px',
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      <Thumb liked>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-5 w-5"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                          onClick={() => onSubmitLikeCommentReply(reply?.id)}
                        >
                          <path d="M2 10.5a1.5 1.5 0 113 0v6a1.5 1.5 0 01-3 0v-6zM6 10.333v5.43a2 2 0 001.106 1.79l.05.025A4 4 0 008.943 18h5.416a2 2 0 001.962-1.608l1.2-6A2 2 0 0015.56 8H12V4a2 2 0 00-2-2 1 1 0 00-1 1v.667a4 4 0 01-.8 2.4L6.8 7.933a4 4 0 00-.8 2.4z" />
                        </svg>
                      </Thumb>
                    </div>
                  ) : (
                    <div
                      onClick={() => onSubmitLikeCommentReply(reply?.id)}
                      style={{
                        padding: '5px',
                        display: 'flex',
                        alignItems: 'center',
                      }}
                    >
                      <Thumb>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          strokeWidth={2}
                          onClick={() => onSubmitLikeCommentReply(reply?.id)}
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            d="M14 10h4.764a2 2 0 011.789 2.894l-3.5 7A2 2 0 0115.263 21h-4.017c-.163 0-.326-.02-.485-.06L7 20m7-10V5a2 2 0 00-2-2h-.095c-.5 0-.905.405-.905.905 0 .714-.211 1.412-.608 2.006L7 11v9m7-10h-2M7 20H5a2 2 0 01-2-2v-6a2 2 0 012-2h2.5"
                          />
                        </svg>
                      </Thumb>
                    </div>
                  )}{' '}
                  <span style={{ color: colors.text }}>|</span>
                  <span>.</span>
                  <span style={{ color: colors.text }}>
                    {reply.likes.length} likes
                  </span>
                </span>
              </OneReply>
            ))}
        </Replyheader>
      </div>
    </>
  )
}

export default memo(commentbox)
