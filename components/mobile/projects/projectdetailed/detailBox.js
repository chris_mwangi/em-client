/* eslint-disable no-unused-expressions */
import { useRouter } from 'next/router'
import Image from 'next/image'
import S3 from 'react-aws-s3'

import React, { useCallback, useContext, useEffect, useRef, useState } from 'react'
import {
  BsFillPlayFill,
  BsFillArrowUpCircleFill,
  BsFileEarmarkImage,
} from 'react-icons/bs'
import { useFormik } from 'formik'
import { HiVolumeUp, HiVolumeOff } from 'react-icons/hi'
import { useMutation } from '@apollo/client'
import styled, { ThemeContext } from 'styled-components'
import { Swiper, SwiperSlide } from 'swiper/react'
import SwiperCore, { Navigation, Pagination } from 'swiper'
import { NormalText } from '../../about'
import CompanyCard from '../companyCard'
import { openInNewTab } from '../../../../libs/helpers'
import CommentBox from './commentBox'
import { useGlobalContext } from '../../../../context/provider'
import {
  COMMENT_PROJECT,
  COMMENT_REPLY_PROJECT,
} from '../../../../apollo/mutations/comment'
import { CLOSE_LOADING, OPEN_ERROR, OPEN_LOADING } from '../../../../constants'
import useAuthStore from '../../../../zustandStore/auth'

SwiperCore.use([Navigation])

const Wrapper = styled.div`
  position: relative;
`
const CompanyWrap = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
  background-color: ${({ theme }) => theme.colors.layout};
`

const WrapperImg = styled.div`
  height: 14rem;
  border-radius: 8px;
  position: relative;
  z-index: 9;
`
const Img = styled(Image)`
  border-radius: 5px;
`
const Wrap = styled.div`
  position: relative;
`

/* const Switch = styled.span`
  padding: 5px 6px;
  color: ${(props) => (props.active === true ? 'red' : 'black')};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: 11px;
  margin-right: 9px;
  // border: 1px solid ${({ theme }) => theme.colors.secondary};
  border-radius: 35px;
  background-color: transparent;
  color: ${({ theme }) => theme.colors.text};
  
  background: rgb(242, 255, 246);
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(0px);
  -webkit-backdrop-filter: blur(0px);
` */
const Video = styled.video`
  width: 99%;
  height: 100%;
  background: rgba(0, 0, 0, 0.9);
`
// const Wrapper = styled.div``

const IconHolder = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  .icon {
    font-size: 73px;
    color: #fff;
  }
`
const IconHolderMute = styled.div`
  position: absolute;
  right: 5%;
  bottom: 7%;

  .icon {
    font-size: 25px;
    color: #fff;
  }
`

const Tab = styled.div`
  display: flex;
  background-color: ${({ theme }) => theme.colors.layout};
  width: 100%;
  .active {
    border-bottom: 1px solid ${({ theme }) => theme.colors.primary};
    color: ${({ theme }) => theme.colors.primary};
  }
  .inactive {
    border-bottom: 1px solid #f1f1f1;
    color: ${({ theme }) => theme.colors.text};
  }
`
const DetailsWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex-basis: 50%;
  padding: 13px 8px;
`
const CommentsWrapper = styled.div`
  display: flex;
  flex-basis: 50%;
  justify-content: center;
  padding: 13px 8px;
`

const Pcomment = styled.span`
  font-size: 15px;
  font-weight: 600;
  font-family: ${({ theme }) => theme.fonts.Apple};
`
const Pdetails = styled.span`
  font-size: 15px;
  font-weight: 600;
  font-family: ${({ theme }) => theme.fonts.Apple};
`
/* const CommentsDiv = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
` */
const DetailsDiv = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
`

const DetailsCommentsWrapper = styled.div`
  overflow-x: hidden;
  background-color: ${({ theme }) => theme.colors.layout};
  margin-top: 8px;
  min-height: 35vh;
  overflow-y: auto;
  padding-bottom: 27px;
  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`
const DownloadLinks = styled.div`
  margin-top: 11px;
  display: grid;
  grid-template-columns: 4fr 1fr 4fr;
  align-items: center;
`

const CommentForm = styled.div`
  position: fixed;

  bottom: 0;
  left: 0;
  width: 100%;
  display: flex;
  align-items: center;
  padding: 7px 13px;
  background-color: ${({ theme }) => theme.colors.layout};
  z-index: 100;
  border-top: 1px solid #eee;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.07), 0 2px 4px rgba(0, 0, 0, 0.07),
    0 4px 8px rgba(0, 0, 0, 0.07), 0 8px 16px rgba(0, 0, 0, 0.07),
    0 16px 32px rgba(0, 0, 0, 0.07), 0 32px 64px rgba(0, 0, 0, 0.07);
`
const AssetWrapper = styled.div`
  background-color: ${({ theme }) => theme.colors.layout};
  width: 100%;
  position: absolute;
  bottom: 3.1rem;
  left: 0;
  z-index: 50;
  padding: 5px 3px;
  border-top: 1px solid ${({ theme }) => theme.colors.graySlight};
`
const AssetDisplay = styled.div`
  border-radius: 8px;
  position: relative;
  width: 30%;
  background-color: ${({ theme }) => theme.colors.layout};
  border: 1px solid ${({ theme }) => theme.colors.graySlight};
`
const CancelImg = styled.div`
  position: absolute;
  top: -16px;
  right: -10px;
  // box-shadow: 0 10px 30px rgba(0, 0, 0, 0.4);
`
const Name = styled.span`
  background: rgba(69, 174, 202, 0.06);
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 3px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const InputWrapper = styled.div`
  margin-left: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  z-index: 100;
`
const Input = styled.input`
  border: 1px solid ${({ theme }) => theme.colors.graySlight};
  outline: none;
  background: ${({ theme }) => theme.colors.inputBg};
  height: 37px;
  padding: 0 6px 0 6px;
  border-radius: 35px;
  font-size: 14px;
  padding-left: 15px;
  color: ${({ theme }) => theme.colors.text};
  flex: 1;
  z-index: 100;
`

const Stack = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  color: ${({ theme }) => theme.colors.text};
  font-weight: 600;
`
const StackWrapper = styled.div``

const Li = styled.li`
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  color: ${({ theme }) => theme.colors.text};
  text-decoration: underline;
`

function DetailBox({ project }) {
  const router = useRouter()
  const [active, setActive] = useState(router.query.action || 'details')
  const { errorDispatch, loadingDispatch } = useGlobalContext()
  const { userProfile } = useAuthStore()
  const [cLength, setCLength] = useState(0)

  const [playing, setPlaying] = useState(false)
  const [isVideoMuted, setIsVideoMuted] = useState(false)
  const { colors, gutters } = useContext(ThemeContext)

  const [replyCommentObj, setReplyCommentObj] = useState({})

  const videoRef = useRef(null)
  const inputRef = useRef(null)

  const config = {
    bucketName: 'playback-wholesale',
    dirName: 'resellerVideos',
    region: 'us-east-1',
    accessKeyId: 'AKIA4GYANEY3DEYWDDSK',
    secretAccessKey: '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',
    s3Url: 'https://playback-wholesale.s3.amazonaws.com',
  }

  const ReactS3Client = new S3(config)

  const [createComment] = useMutation(COMMENT_PROJECT, {
    onError() {
      errorDispatch({
        type: OPEN_ERROR,
        payload: 'Server error',
      })
    },
  })

  const [createCommentReply] = useMutation(COMMENT_REPLY_PROJECT, {
    onError() {
      errorDispatch({
        type: OPEN_ERROR,
        payload: 'Server error',
      })
    },
  })

  const onSubmitComment = async (values) => {
    try {
      loadingDispatch({
        type: OPEN_LOADING,
      })
      const {
        data: {
          createComment: { projects, errors },
        },
      } = await createComment({
        variables: { ...values },
      })
      if (errors) {
        const errorMessage = errors?.message
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }
      loadingDispatch({
        type: CLOSE_LOADING,
      })
      if (projects) {
        formik.resetForm()
        loadingDispatch({
          type: CLOSE_LOADING,
        })
      }
    } catch (error) {
      return loadingDispatch({
        type: CLOSE_LOADING,
      })
    }
    return {}
  }

  const onSubmitCommentReply = async (values) => {
    try {
      loadingDispatch({
        type: OPEN_LOADING,
      })
      const {
        data: {
          createCommentReply: { projects, errors },
        },
      } = await createCommentReply({
        variables: { ...values },
      })
      if (errors) {
        const errorMessage = errors?.message
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }
      loadingDispatch({
        type: CLOSE_LOADING,
      })
      if (projects) {
        formikReply.resetForm()
        loadingDispatch({
          type: CLOSE_LOADING,
        })
      }
    } catch (error) {
      return loadingDispatch({
        type: CLOSE_LOADING,
      })
    }
    return {}
  }

  const handleOpenFileInput = () => {
    inputRef.current.click()
  }
  const formik = useFormik({
    initialValues: {
      projectId: project[0]?.id,
      body: '',
      assetUrl: '',
    },
    enableReinitialize: true,
    // validationSchema: LOGIN_SCHEMA,

    onSubmit: (values) => {
      onSubmitComment(values)
    },
  })

  const formikReply = useFormik({
    initialValues: {
      projectId: project[0]?.id,
      commentId: replyCommentObj?.id,
      body: '',
      assetUrl: '',
    },
    enableReinitialize: true,
    // validationSchema: LOGIN_SCHEMA,

    onSubmit: (values) => {
      onSubmitCommentReply(values)
    },
  })

  const UploadVideo = async (e) => {
    loadingDispatch({
      type: OPEN_LOADING,
    })
    try {
      const fileInp = e.target.files[0]
      const fileName = e.target.files[0].name
      const fileUp = await ReactS3Client.uploadFile(fileInp, fileName)

      if (fileUp.status === 204) {
        loadingDispatch({
          type: CLOSE_LOADING,
        })
        Object.values(replyCommentObj).length > 0
          ? formikReply.setFieldValue('assetUrl', fileUp.location)
          : formik.setFieldValue('assetUrl', fileUp.location)
      }
    } catch (err) {
      loadingDispatch({
        type: CLOSE_LOADING,
      })
      errorDispatch({
        type: OPEN_ERROR,
        payload: 'Server error',
      })
    }
  }

  /*   const handleExitViewport = () => {
    videoRef?.current?.pause()
    setPlaying(false)
  } */

  const memoizedCallback = useCallback((state) => {
    setActive(state)
  }, [])

  const commentlength = () => {
    const comments = project[0]?.comments?.length
    const replies = project[0]?.comments.map((comment) => comment.replies.length)
    const all = replies.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      0
    )
    return setCLength(comments + all)
  }
  useEffect(() => {
    commentlength()
  }, [project])

  const onVideoPress = () => {
    if (playing) {
      videoRef?.current?.pause()
      setPlaying(false)
    } else {
      videoRef?.current?.play()
      setPlaying(true)
    }
  }
  useEffect(() => {
    if (videoRef?.current) {
      videoRef.current.muted = isVideoMuted
    }
  }, [isVideoMuted])
  return (
    <Wrapper>
      <CompanyWrap>
        <CompanyCard project={project && project[0]} />
        <NormalText
          style={{ fontSize: '14px', lineHeight: '24px', marginTop: '9px' }}
        >
          {project[0]?.description}
        </NormalText>
      </CompanyWrap>
      {/*  <NormalText
        dangerouslySetInnerHTML={{ __html: project[0]?.detailedDescription }}
      /> */}

      <Wrap>
        <Name />

        <Swiper
          modules={[Navigation, Pagination]}
          slidesPerView={1}
          spaceBetween={2}
          controller
          pagination={{
            clickable: true,
            dynamicBullets: true,
            dynamicMainBullets: 7,
          }}
          centeredSlides
          centeredSlidesBounds
        >
          {project[0].projectImages?.map((image) => (
            <SwiperSlide key={image}>
              <WrapperImg>
                <Img
                  src={image}
                  objectFit="cover"
                  quality={100}
                  alt=".."
                  layout="fill"
                />
              </WrapperImg>
            </SwiperSlide>
          ))}
        </Swiper>

        {/* <SwitchWrapper>
          <Switch
            onClick={() => memoizedCallback('images')}
            className={active === 'images' ? 'active' : 'inactive'}
          >
            Images
          </Switch>
          <Switch
            onClick={() => memoizedCallback('videos')}
            className={active === 'videos' ? 'active' : 'inactive'}
          >
            Video
          </Switch>
        </SwitchWrapper> */}
      </Wrap>
      <DetailsCommentsWrapper>
        <Tab>
          <DetailsWrapper
            className={active === 'details' ? 'active' : 'inactive'}
            onClick={() => memoizedCallback('details')}
          >
            <Pdetails>Details</Pdetails>
          </DetailsWrapper>
          <CommentsWrapper className={active === 'comments' ? 'active' : 'inactive'}>
            <Pcomment onClick={() => memoizedCallback('comments')}>
              Comments ({cLength})
            </Pcomment>
          </CommentsWrapper>
        </Tab>
        {active === 'comments' ? (
          <div style={{ marginBottom: '45px', marginTop: '6px' }}>
            {project[0]?.comments?.map((comment) => (
              <CommentBox
                setReplyCommentObj={setReplyCommentObj}
                replyCommentObj={replyCommentObj}
                comment={comment}
                id={project[0]?.id}
              />
            ))}
            <CommentForm>
              {formik.values.assetUrl !== '' ||
                (formikReply.values.assetUrl !== '' && (
                  <AssetWrapper>
                    <AssetDisplay>
                      {formik.values.assetUrl ? (
                        <Image
                          src={formik.values.assetUrl}
                          objectFit="contain"
                          height={44}
                          style={{ marginTop: '3px' }}
                          width="100%"
                        />
                      ) : (
                        <Image
                          src={formikReply.values.assetUrl}
                          objectFit="contain"
                          height={44}
                          style={{ marginTop: '3px' }}
                          width="100%"
                        />
                      )}
                      <CancelImg
                        onClick={() => {
                          formik.setFieldValue('assetUrl', '')
                          formikReply.setFieldValue('assetUrl', '')
                        }}
                      >
                        <Image
                          height={38}
                          width={38}
                          src="/assets/mobile/images/close.svg"
                          style={{ zIndex: 300 }}
                        />
                      </CancelImg>
                      <Name />
                    </AssetDisplay>
                  </AssetWrapper>
                ))}

              <Img
                style={{ borderRadius: '50%' }}
                src={userProfile?.image}
                height={30}
                width={30}
                objectFit="contain"
              />
              <InputWrapper>
                <input
                  hidden
                  ref={inputRef}
                  type="file"
                  id="avatar"
                  name="avatar"
                  accept="image/*"
                  onChange={UploadVideo}
                />
                <BsFileEarmarkImage
                  onClick={handleOpenFileInput}
                  style={{ marginRight: '6px' }}
                  fontSize={gutters.large}
                  color={colors.text}
                />
                {Object.values(replyCommentObj).length > 0 ? (
                  <Input
                    placeholder={`Reply to ${replyCommentObj.userName}`}
                    name="body"
                    onChange={formikReply.handleChange}
                    value={formikReply.values.body}
                    autoComplete="off"
                  />
                ) : (
                  <Input
                    placeholder="Leave your thoughts 👍..."
                    name="body"
                    onChange={formik.handleChange}
                    value={formik.values.body}
                    autoComplete="off"
                  />
                )}

                {Object.values(replyCommentObj).length > 0 ? (
                  <form onSubmit={formikReply.handleSubmit}>
                    <button
                      type="submit"
                      style={{
                        border: 'none',
                        background: 'transparent',
                      }}
                    >
                      <BsFillArrowUpCircleFill
                        color={colors.primary}
                        fontSize={gutters.xlarge}
                      />
                    </button>
                  </form>
                ) : (
                  <form onSubmit={formik.handleSubmit}>
                    <button
                      type="submit"
                      style={{
                        border: 'none',
                        backgroundColor: 'transparent',
                      }}
                    >
                      <BsFillArrowUpCircleFill
                        color={colors.primary}
                        fontSize={gutters.xlarge}
                      />
                    </button>
                  </form>
                )}

                {/*       {formikReply.values.body && (
                  <form onSubmit={formikReply.handleSubmit}>
                    <button
                      type="submit"
                      style={{
                        border: 'none',
                        backgroundColor: 'transparent',
                      }}
                    >
                      <BsFillArrowUpCircleFill
                        color={colors.primary}
                        fontSize={gutters.xlarge}
                      />
                    </button>
                  </form>
                )} */}
              </InputWrapper>
            </CommentForm>
          </div>
        ) : (
          <DetailsDiv>
            <NormalText
              dangerouslySetInnerHTML={{ __html: project[0]?.detailedDescription }}
            />
            {project[0]?.projectVideos.length > 0 && (
              <WrapperImg style={{ marginTop: '-46px' }}>
                <Video
                  onClick={onVideoPress}
                  loop
                  playing={playing}
                  ref={videoRef}
                  id="video"
                  src={project[0].projectVideos[0]}
                />
                <IconHolder onClick={onVideoPress}>
                  <div>
                    {!playing && (
                      <span>
                        <BsFillPlayFill className="icon" />
                      </span>
                    )}
                  </div>
                </IconHolder>
                <IconHolderMute onClick={() => setIsVideoMuted(!isVideoMuted)}>
                  {isVideoMuted ? (
                    <span>
                      <HiVolumeOff className="icon" />
                    </span>
                  ) : (
                    <span>
                      <HiVolumeUp className="icon" />
                    </span>
                  )}
                </IconHolderMute>
              </WrapperImg>
            )}
            <div style={{ marginTop: '12px' }}>
              <Stack>Tech stack used</Stack>
              <StackWrapper>
                {project[0]?.techStack?.map((pr) => (
                  <ul key={pr.id}>
                    <Li onClick={() => openInNewTab(pr.techLink)}>{pr.techName}</Li>
                  </ul>
                ))}
              </StackWrapper>
            </div>
            <DownloadLinks>
              <button
                onClick={() => openInNewTab(project[0]?.appstoreUrl)}
                style={{
                  border: 'none',
                  outline: 'none',
                  background: 'transparent',
                }}
                type="button"
              >
                <img
                  alt="ios link"
                  src="/assets/mobile/images/ios.png"
                  style={{
                    height: '50px',
                    width: '100%',
                    objectFit: 'contain',
                  }}
                />
              </button>
              <div />
              <button
                onClick={() => openInNewTab(project[0]?.playstoreUrl)}
                style={{
                  border: 'none',
                  outline: 'none',
                  background: 'transparent',
                }}
                type="button"
              >
                <img
                  alt="ios link"
                  src="/assets/mobile/images/android.png"
                  style={{ height: '36px', width: '100%', objectFit: 'contain' }}
                />
              </button>
            </DownloadLinks>
          </DetailsDiv>
        )}
      </DetailsCommentsWrapper>
    </Wrapper>
  )
}

export default DetailBox
