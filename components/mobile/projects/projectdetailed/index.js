import { useQuery } from '@apollo/client'
import { useRouter } from 'next/router'
import styled from 'styled-components'
import { FIND_MY_SINGLE_PROJECT } from '../../../../apollo/queries/projects'
import { OPEN_ERROR } from '../../../../constants'
import { useGlobalContext } from '../../../../context/provider'

import DetailBox from './detailBox'
import Loader from './loader'

const MobileEntryWrapper = styled.div`
  margin-top: 55px;
  width: 100%;

  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`
const WrapperBox = styled.div`
  // background-color: ${({ theme }) => theme.colors.layout};
  margin-top: ${({ theme }) => theme.gutters.xsmall};
`
/* const Text = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
` */

const index = () => {
  const router = useRouter()
  const { project } = router.query
  const { errorDispatch } = useGlobalContext()

  const { data, loading } = useQuery(FIND_MY_SINGLE_PROJECT, {
    variables: { projectId: project },
  })

  const projectData = data?.getProject?.projects
  const projectErrors = data?.getProject?.errors

  if (projectErrors) {
    errorDispatch({
      type: OPEN_ERROR,
      payload: projectErrors.message,
    })
  }

  if (loading) {
    return (
      <div style={{ overFlow: 'hidden' }}>
        <Loader />
      </div>
    )
  }

  return (
    <MobileEntryWrapper style={{ marginTop: '63px' }}>
      <WrapperBox>
        <DetailBox project={projectData} />
      </WrapperBox>
    </MobileEntryWrapper>
  )
}

export default index
