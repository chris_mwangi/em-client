import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  width: 100vw;
  margin-bottom: -15px;
  padding: 3px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
  overflow-x: scroll;
  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`
const Filterbox = styled.div`
  background: ${({ theme }) => theme.colors.layout};
  padding: 7px 17px;
  border-radius: ${({ theme }) => theme.gutters.medium};
  border: 1px solid ${({ theme }) => theme.colors.graySlight};
  margin-right: 10px;
`
const Filtertext = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-size: 12px;
  font-family: ${({ theme }) => theme.fonts.Apple};
`

function Filters() {
  return (
    <Wrapper>
      {[1, 2, 3, 4, 5, 6, 7, 8].map(() => (
        <Filterbox>
          <Filtertext>React</Filtertext>
        </Filterbox>
      ))}
    </Wrapper>
  )
}

export default Filters
