import { memo, useContext } from 'react'
import { useRouter } from 'next/router'
import { BsThreeDotsVertical } from 'react-icons/bs'
import Image from 'next/image'
import styled, { ThemeContext } from 'styled-components'
import { BoldText } from '../about/index'

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const ProfileWrapper = styled.div`
  display: flex;
  row-gap: 10px;
  column-gap: 13px;
`
const NameWrapper = styled.div`
  display: flex;
  // row-gap: 20px;
  flex-direction: column;
`
const Img = styled(Image)``

const TinyTxt = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.small};
  line-height: ${({ theme }) => theme.gutters.large};
`
function companyCard({ project }) {
  const { gutters } = useContext(ThemeContext)
  const router = useRouter()
  const onClick = (url) => {
    router.push(url)
  }

  return (
    <Wrapper>
      <ProfileWrapper>
        <Img
          quality={60}
          src={project?.company?.companyLogo}
          objectFit="contain"
          height={35}
          width={35}
          onClick={() => onClick(`/company/${project.company.id} `)}
        />
        <NameWrapper>
          <BoldText>By,{project?.company?.companyName}</BoldText>
          <TinyTxt>{project?.company?.companyAddress}</TinyTxt>
        </NameWrapper>
      </ProfileWrapper>
      <BsThreeDotsVertical fontSize={gutters.large} />
    </Wrapper>
  )
}

export default memo(companyCard)
