/* eslint-disable no-unsafe-optional-chaining */
import Image from 'next/image'
import styled from 'styled-components'
import useAuthStore from '../../../zustandStore/auth'
import { BoldText } from '../about/education'

const TinyTxt = styled.p`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2; /* number of lines to show */
  line-clamp: 2;
  -webkit-box-orient: vertical;
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: 14px;
  line-height: ${({ theme }) => theme.gutters.large};
`

const Wrapper = styled.div`
  padding: 12px 0;
`
const LikesCommentsWrapperCount = styled.div`
  display: flex;
  justify-content: space-between;
  font-family: ${({ theme }) => theme.fonts.Apple};
  align-items: center;
  margin-top: 5px;
  margin-left: 8px;
`
const Images = styled.div`
  display: flex;
`
const TextCounts = styled.div`
  margin-right: 9px;
`
const Img = styled.div`
  height: 23px;
  width: 23px;
  border-radius: 50%;
  margin-left: -11px;
  background-color: ${({ theme }) => theme.colors.primary};
  display: flex;
  align-items: center;
  justify-content: center;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.03), 0 2px 4px rgba(0, 0, 0, 0.03),
    0 4px 8px rgba(0, 0, 0, 0.03), 0 8px 16px rgba(0, 0, 0, 0.03),
    0 16px 32px rgba(0, 0, 0, 0.03), 0 32px 64px rgba(0, 0, 0, 0.03);
`

const likesCommentsCard = ({ project }) => {
  const { userProfile } = useAuthStore()

  function lastLike(array) {
    if (array.find((like) => like?.userName === userProfile?.displayName)) {
      return 'You'
    }
    return array[0]?.userName === userProfile?.displayName
      ? 'You'
      : array[0]?.userName
  }
  return (
    <Wrapper>
      <BoldText>
        {project?.projectTitle},,,{project?.isMobile ? '(Mobile)' : '(Web)'}
      </BoldText>
      <TinyTxt>{project?.description}</TinyTxt>
      <LikesCommentsWrapperCount>
        <Images>
          {project.projectlikes.length > 0 &&
            project.projectlikes?.slice(0, 4)?.map((like) => (
              <Img>
                <Image
                  style={{ borderRadius: '50%' }}
                  objectFit="contain"
                  src={like?.profilePicUrl}
                  height={21}
                  width={21}
                />
              </Img>
            ))}
        </Images>
        <TextCounts>
          {project.projectlikes.length === 1 && (
            <TinyTxt>{`${lastLike(project.projectlikes)} liked`}</TinyTxt>
          )}
          {project.projectlikes.length > 1 && (
            <TinyTxt>{`${lastLike(project.projectlikes)} liked and ${
              project.projectlikes.length - 1
            } ${project.projectlikes.length > 2 ? 'others' : 'other'}`}</TinyTxt>
          )}
        </TextCounts>
      </LikesCommentsWrapperCount>
    </Wrapper>
  )
}

export default likesCommentsCard
