import Image from 'next/image'
import { useEffect, useRef } from 'react'
import styled from 'styled-components'
import useAuthStore from '../../../zustandStore/auth'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: ${({ theme }) => theme.gutters.regular};
  margin-bottom: 3.5rem;
  overflow-y: auto;
`
const Bubble = styled.div`
  max-width: 70vw;
  background-color: #fff;
  width: fit-content;
  min-width: 40vw;
  padding: 8px ${({ theme }) => theme.gutters.regular};
  border-top-right-radius: 35px;
  border-bottom-right-radius: 35px;
  border-bottom-left-radius: 35px;
  color: ${({ theme }) => theme.colors.text};
`
const BubbleOwner = styled(Bubble)`
  background-color: #f4e2cb;
  align-self: flex-end;
  border-top-right-radius: 0;
  border-bottom-right-radius: 35px;
  border-bottom-left-radius: 35px;
  border-top-left-radius: 35px;
`
const P = styled.p`
  // font-family: ${({ theme }) => theme.fonts.Apple};
  font-family: 'Nunito-Regular';
  font-size: ${({ theme }) => theme.gutters.medium};
`

const Profile = styled.div`
  display: flex;
  align-items: center;
  column-gap: ${({ theme }) => theme.gutters.tiny};
  margin-bottom: ${({ theme }) => theme.gutters.tiny};
  p {
    font-family: ${({ theme }) => theme.fonts.Apple};
    font-size: ${({ theme }) => theme.gutters.small};
    font-weight: 600;
  }
`
const ProfileSender = styled(Profile)`
  justify-content: flex-end;
`
const Time = styled.p`
  font-size: 11px;
  font-family: ${({ theme }) => theme.fonts.Apple};
  float: right;
  clear: both;
  margin: 8px 0;
`
const TimeSender = styled.p`
  font-size: 11px;
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 8px 0;
`

function ChatBubble({ messages }) {
  const { userProfile } = useAuthStore()

  console.log(userProfile)

  const scrollRef = useRef()

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ bahaviour: 'smooth' })
  }, [])

  return (
    <Wrapper>
      {messages?.map((message) => (
        <div>
          {message.sender.id === userProfile.id ? (
            <BubbleOwner>
              <ProfileSender>
                <p>{message.from.displayName} </p>
                <Image
                  width={21}
                  height={21}
                  style={{ borderRadius: '50%' }}
                  alt="..."
                  src={message.sender.image}
                />
              </ProfileSender>
              <P>
                <P>{message.message}</P>
              </P>
              <TimeSender>11:56 pm</TimeSender>
            </BubbleOwner>
          ) : (
            <Bubble>
              <Profile>
                <Image
                  width={21}
                  height={21}
                  style={{ borderRadius: '50%' }}
                  alt="..."
                  src={message?.recipient.image}
                />
                <p>{message?.recipient.displayName}</p>
              </Profile>
              <P>{message.message}</P>
              <Time>11:40 pm</Time>
            </Bubble>
          )}
        </div>
      ))}
    </Wrapper>
  )
}

export default ChatBubble
