/* eslint-disable no-unsafe-optional-chaining */
import { useMutation, useQuery } from '@apollo/client'
import React, { useState } from 'react'
import styled from 'styled-components'
import { GET_MESSAGES } from '../../../apollo/queries/messages'
import ChatBubble from './chatBubble'
import SendForm from './sendForm'
import { OPEN_ERROR } from '../../../constants'
import { useGlobalContext } from '../../../context/provider'
import { SEND_MESSAGE } from '../../../apollo/mutations/sendMessage'

const Wrapper = styled.div`
  background-image: url('./assets/mobile/images/chat_bg.png');
  height: 100vh;
  overflow-y: auto;
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
`

function Index() {
  const [messages, setMessages] = useState([])
  console.log(messages)

  const { errorDispatch } = useGlobalContext()

  const [sendMessage] = useMutation(SEND_MESSAGE)

  const { loading } = useQuery(GET_MESSAGES, {
    fetchPolicy: 'cache-and-network',
    variables: {
      from: '63015e89f9c3b8d817fe79cb',
    },
    onCompleted: (res) => {
      const { getMessages } = res

      setMessages([...getMessages?.messages])
    },
    onError: () => {
      errorDispatch({
        type: OPEN_ERROR,
        payload: 'Server Error!',
      })
    },
  })

  if (loading) {
    return <p>Loading</p>
  }

  const onSubmit = async () => {
    try {
      const {
        data: {
          sendMessage: { messages, errors },
        },
      } = await sendMessage({
        variables: {
          to: '631f95edf166edaebdd34bf6',
          message: 'new',
          imageUrl: '',
        },
      })
      if (messages) {
        setMessages((prevMessages) => [...prevMessages, messages[0]])
      }
      if (errors) {
        const errorMessage = errors?.message
        errorDispatch({
          type: OPEN_ERROR,
          payload: errorMessage,
        })
      }
    } catch (error) {
      console.log(error)
    }
    return {}
  }

  return (
    <Wrapper>
      <ChatBubble messages={messages} />
      <SendForm onSubmit={onSubmit} />
    </Wrapper>
  )
}

export default Index
