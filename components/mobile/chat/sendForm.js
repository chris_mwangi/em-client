/* eslint-disable no-unused-vars */
import Image from 'next/image'
import React, { useRef } from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  height: 3.6rem;
  background-image: url('./assets/mobile/images/chat_bg.png');
  display: flex;
  align-items: center;
  position: fixed;
  width: 100%;
  bottom: 0;
  left: 0;
  background-color: ${({ theme }) => theme.colors.background};
  padding: 8px ${({ theme }) => theme.gutters.medium};
`
const InputWrapper = styled.div`
  display: flex;
  width: 100vw;
  justify-content: center;
`

const Input = styled.input`
  border: none;
  outline: none;
  width: 100%;

  ::placeholder {
    color: ${({ theme }) => theme.colors.text};
  }
`
const Inp = styled.div`
  padding: 7px 14px;
  border-radius: 31px;
  box-shadow: 0px 3px 6px #00000029;
  background: ${({ theme }) => theme.colors.layout};
  display: flex;
  flex-basis: 80%;
`
const Send = styled.div`
  position: relative;
  height: 1.7rem;
  width: 2.2rem;
`
const IcWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-basis: 20%;
`
const Icon = styled.div`
  // flex-basis: 20%;
  align-items: center;
  justify-content: center;
  width: 2.1rem;
  height: 2.1rem;
  /* UI Properties */
  background: transparent linear-gradient(180deg, #ec1b25 0%, #ea1977 100%) 0% 0%
    no-repeat padding-box;
  box-shadow: 0px 3px 6px #00000029;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
`
function SendForm({ onSubmit = () => {} }) {
  const nameRef = useRef()

  const focus = () => {
    nameRef.current.focus()
  }
  return (
    <Wrapper>
      <InputWrapper>
        <Inp>
          <Input ref={nameRef} placeholder="Message" />
          <Send onClick={onSubmit}>
            <Image layout="fill" src="/assets/mobile/images/send_2.png" alt="Send" />
          </Send>
        </Inp>
        <IcWrap>
          <Icon>
            <img
              src="/assets/mobile/images/paper_clip.png"
              alt="..."
              style={{ width: '1rem', height: '1rem' }}
            />
          </Icon>
        </IcWrap>
      </InputWrapper>
    </Wrapper>
  )
}

export default SendForm
