import { useRouter } from 'next/router'
import { useQuery } from '@apollo/client'
import React from 'react'
import styled from 'styled-components'
import Loader from './loader'
import { GET_COMPANY } from '../../../apollo/queries/companies'
import Banner from './banner'
import Desc from './desc'
import About from './about-us'

const WrapperBox = styled.div`
  // background-color: ${({ theme }) => theme.colors.layout};
  /*   padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium}; */
  margin-top: ${({ theme }) => theme.gutters.xsmall};
  margin-bottom: 64px;
`

const Wrap = styled.div`
  background-color: ${({ theme }) => theme.colors.layout};
  padding-bottom: 8px;
  margin-top: ${({ theme }) => theme.gutters.xsmall};
`

const index = () => {
  const router = useRouter()
  const { data, loading } = useQuery(GET_COMPANY, {
    variables: {
      companyId: router?.query.id,
    },
  })
  const company = data?.getCompany.companies
  if (loading) {
    return <Loader />
  }

  return (
    <WrapperBox>
      <Wrap>
        <Banner data={company[0]} />
        <Desc data={company[0]} />
      </Wrap>
      <Wrap>
        <About data={company[0]} />
      </Wrap>
    </WrapperBox>
  )
}

export default index
