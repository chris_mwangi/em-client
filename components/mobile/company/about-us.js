import { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { openInNewTab } from '../../../libs/helpers'

const Wrapper = styled.div``

const AboutWrapper = styled.div`
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.graySlight};
  padding: 11px ${({ theme }) => theme.gutters.medium};
`
const BoldTextRegular = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-weight: 600;
  font-size: 14px;
  color: ${({ theme }) => theme.colors.text};
`

const NormalText = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  line-height: ${({ theme }) => theme.gutters.large};
`
const TinyText = styled.p`
  color: rgba(0, 0, 0, 0.6);
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.small};
  line-height: ${({ theme }) => theme.gutters.large};
`
const Main = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.medium};
`
const Contain = styled.div`
  margin: 8px 0;
`

function About({ data }) {
  const { colors } = useContext(ThemeContext)
  return (
    <Wrapper>
      <AboutWrapper>
        <BoldTextRegular>About {data?.companyName}</BoldTextRegular>
      </AboutWrapper>
      <Main>
        <NormalText>{data?.companyAbout}</NormalText>
        <Contain>
          <TinyText>Headquaters</TinyText>
          <NormalText>{data?.companyAddress}</NormalText>
        </Contain>
        <Contain>
          <TinyText>Company size</TinyText>
          <NormalText>{data?.companySize}</NormalText>
        </Contain>
        <Contain>
          <TinyText>Website</TinyText>
          <NormalText
            style={{ color: colors.link }}
            onClick={() => openInNewTab(data?.data?.companyWebsite)}
          >
            {data?.companyWebsite}
          </NormalText>
        </Contain>
        <Contain>
          <TinyText>Industry</TinyText>
          <NormalText>{data?.companyField}</NormalText>
        </Contain>
      </Main>
    </Wrapper>
  )
}

export default About
