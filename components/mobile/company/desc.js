import React, { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { FiPlus } from 'react-icons/fi'
import { openInNewTab } from '../../../libs/helpers'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`
const Name = styled.p`
  margin-top: ${({ theme }) => theme.gutters.xsmall};
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.regular};
  line-height: ${({ theme }) => theme.gutters.xxlarge};
  // font-weight: 600;
`

const TinyTxt = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  // line-height: ${({ theme }) => theme.gutters.large};
`
const Holder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`
const Btn = styled.div`
  display: flex;
  align-items: center;
  border: 1px solid ${({ theme }) => theme.colors.link};
  padding: 6px ${({ theme }) => theme.gutters.large};
  color: ${({ theme }) => theme.colors.link};
  border-radius: 22px;
  margin-top: ${({ theme }) => theme.gutters.medium};
`
const Button = styled.span`
  outline: none;
  font-family: ${({ theme }) => theme.fonts.Apple};
  background-color: ${({ theme }) => theme.colors.layout};
  font-size: ${({ theme }) => theme.gutters.medium};
`

function Desc({ data }) {
  const { colors, gutters } = useContext(ThemeContext)
  return (
    <Wrapper>
      <Holder>
        <Name>{data?.companyName}</Name>
        <div>
          <TinyTxt>{data?.companyField}.</TinyTxt>
        </div>
        <Btn>
          <FiPlus fontSize={gutters.medium} color={colors.link} />
          <Button onClick={() => openInNewTab(data?.companyLinkedinUrl)}>
            Visit Linkedin
          </Button>
        </Btn>
      </Holder>
    </Wrapper>
  )
}

export default Desc
