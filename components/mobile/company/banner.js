import Image from 'next/image'
import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div``

const BannerImg = styled.div`
  height: 7.7rem;
  position: relative;
`
const LogoImg = styled.div`
  display: flex;
  justify-content: center;
`
const Logo = styled.div`
  height: 3rem;
  width: 3rem;
  position: relative;
  margin-top: -1.5rem;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.07), 0 2px 4px rgba(0, 0, 0, 0.07),
    0 4px 8px rgba(0, 0, 0, 0.07), 0 8px 16px rgba(0, 0, 0, 0.07),
    0 16px 32px rgba(0, 0, 0, 0.07), 0 32px 64px rgba(0, 0, 0, 0.07);
`

function banner({ data }) {
  return (
    <Wrapper>
      <BannerImg>
        <Image
          src={data?.companyCoverImage}
          layout="fill"
          priority
          objectFit="cover"
        />
      </BannerImg>
      <LogoImg>
        <Logo>
          <Image src={data?.companyLogo} layout="fill" objectFit="contain" />
        </Logo>
      </LogoImg>
    </Wrapper>
  )
}

export default banner
