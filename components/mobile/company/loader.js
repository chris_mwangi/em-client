import styled from 'styled-components'
import SkeletonBox from '../../skeleton/SkeletonBox'

const Wrap = styled.div`
  height: 100%;
  overflow: hidden !;
`

const Wrapper = styled.div`
  padding: 8px ${({ theme }) => theme.gutters.medium} 2px
    ${({ theme }) => theme.gutters.medium};
  margin-top: 15px;
`
const CompanyWrapper = styled.div`
  display: grid;
  grid-template-columns: 3fr 10fr;
`
const CompanyLogo = styled.span`
  height: 50px;
  margin-right: 10px;
`
const CompanyWritings = styled.div`
  height: 13px;
`
const ProjectDesc = styled.span`
  display: flex;
  margin-top: 8px;
`
const ProjectCard = styled.div`
  height: 30vh;
  flex-basis: 97%;
  margin-right: 6px;
`
/* const ActionCard = styled.span`
  flex-basis: 6%;
  height: 110px;
  align-self: flex-end;
` */

function Loader() {
  return (
    <Wrap>
      {new Array(3).fill(0).map((_, i) => (
        <Wrapper key={i}>
          <CompanyWrapper>
            <CompanyLogo>
              <SkeletonBox amount={1} />
            </CompanyLogo>
            <CompanyWritings>
              <SkeletonBox amount={1} />
              <div style={{ width: '60%' }}>
                <SkeletonBox amount={1} />
              </div>
            </CompanyWritings>
          </CompanyWrapper>
          <ProjectDesc>
            <ProjectCard>
              <SkeletonBox amount={1} />
            </ProjectCard>
            {/*   <ActionCard>
              <SkeletonBox amount={1} />
            </ActionCard> */}
          </ProjectDesc>
        </Wrapper>
      ))}
    </Wrap>
  )
}

export default Loader
