import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  height: 100vh;
  background-color: #121923;
  overflow: hidden;
  .logo {
    width: 52px;
    height: 52px;
  }
  .loading-scr {
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    position: fixed;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    background-color: #121923;
  }
  .load-bar {
    width: 150px;
    height: 2px;
    position: relative;
    overflow: hidden;
    background: #cfcfcf;
    margin-top: 8px;
  }
  .load-bar::before {
    content: '';
    width: 75px;
    height: 2px;
    position: absolute;
    left: -34px;
    background: #0073b1;
    animation: loadBar 1.5s infinite ease;
  }

  /*-- load bar animation CSS--*/
  @keyframes loadBar {
    50% {
      left: 100px;
    }
  }
`

const LogoName = styled.div`
  font-family: 'Caveat', cursive;
  color: rgb(32, 110, 233);
  font-size: 24px;
  line-height: 23px;
  margin-bottom: 21px;
`

function inLoader() {
  return (
    <Wrapper>
      <div className="loading-scr">
        <div style={{ marginTop: '-2.3rem' }} className="loading-animation">
          <LogoName>Mwangi Maina</LogoName>
          <div className="load-bar" />
        </div>
      </div>
    </Wrapper>
  )
}

export default inLoader
