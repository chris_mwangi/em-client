/* import Image from 'next/image'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 2.6rem 1rem;
  overflow-y: auto;
  overflow-x: hidden;
`
const InnerWrapper = styled.div`
  width: 70vw;
  span {
    color: rgb(204, 214, 246);
    font-family: ${({ theme }) => theme.fonts.Apple};
    line-height: 24px;
    word-spacing: 3px;
    font-size: 22px;
  }
`

const Title = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: rgb(204, 214, 246);
  font-size: 1.6rem;
  margin: 12px 0;
  font-weight: 600;
  margin-bottom: 1.4rem;
`
const Wrap = styled.div`
  margin-bottom: 15px;
`
const CompanyWrapper = styled.div``
const CompanyHead = styled.div`
  display: flex;
  column-gap: 19px;
  margin-bottom: -1rem;
`
const Description = styled.div`
  p {
    font-family: ${({ theme }) => theme.fonts.Apple};
    color: rgb(204, 214, 246);
    font-size: 15px;
    line-height: 24px;
  }
`
const Workdesc = styled.p`
  width: 50vw;
  color: ${({ theme }) => theme.colors.slate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: 24px;
  font-size: 15px;
  margin-top: 1.6rem;
  word-spacing: 4px;
  flex: 1;
  margin-right: 2rem;
`
const DescWrapper = styled.div`
  margin-left: 3rem;
`

function CompanyBox({ exp }) {
  return (
    <CompanyWrapper>
      <CompanyHead>
        <Image src={exp?.company.companyLogo} height={49} width={49} />
        <Description>
          <p>{exp.company.companyName}</p>
          <p>{exp.company.companyAddress}</p>
        </Description>
      </CompanyHead>
      <DescWrapper>
        <ul>
          {exp?.role.map((ex) => (
            <Workdesc>
              <li>{ex.description}</li>
            </Workdesc>
          ))}
        </ul>
      </DescWrapper>
    </CompanyWrapper>
  )
}

const index = ({ data }) => {
  return (
    <Wrapper id="about">
      <InnerWrapper>
        <Title>Where i have worked.</Title>
        {data?.experience?.map((exp) => (
          <Wrap>
            <CompanyBox exp={exp} />
          </Wrap>
        ))}
      </InnerWrapper>
    </Wrapper>
  )
}

export default index
 */

/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable import/no-cycle */
import Image from 'next/image'
// import { useRouter } from 'next/router'
import { BsChevronCompactDown } from 'react-icons/bs'
import { useContext, useState } from 'react'
import styled, { ThemeContext } from 'styled-components'
import { WrapperBox, NormalText, BoldTextRegular } from '../mobile/about'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 2.6rem 1rem;
  overflow-y: auto;
  overflow-x: hidden;
`
const InnerWrapper = styled.div`
  width: 70vw;
  span {
    color: rgb(204, 214, 246);
    font-family: ${({ theme }) => theme.fonts.Apple};
    line-height: 24px;
    word-spacing: 3px;
    font-size: 22px;
  }
`
const Title = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.9rem;
  margin: 12px 0;
  font-weight: 600;
`
const WrapperBoxItem = styled.div`
  display: grid;
  grid-template-columns: 1fr 5fr;
  margin: 1.3rem 0;
  align-items: start;
`

export const BoldText = styled.p`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: ${({ theme }) => theme.gutters.medium};
`
const SchoolLogo = styled.div`
  height: 4rem;
  width: 4rem;
  position: relative;
  background: linear-gradient(
    0deg,
    rgba(242, 255, 246, 1) 0%,
    rgba(255, 241, 248, 1) 100%
  );

  margin-right: ${({ theme }) => theme.gutters.small};
  display: flex;
  align-items: center;
  justify-content: center;
`
const SchoolDesc = styled.div`
  border-bottom: 1px solid ${(props) => props.theme.colors.graySlight};
`

const TinyTxt = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: 16px;
  line-height: 25px;
`
const Text = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: 'Caveat', cursive;
  font-size: 17px;
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  background: rgba(0, 0, 0, 0.001);
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
`
const Expcount = styled.div`
  display: flex;
  .span {
    width: 1px;
    background-color: ${({ theme }) => theme.colors.footerGray};
    margin: 0.18rem 1rem 0.8rem 0;
    &::before {
      content: '';
      display: flex;
      width: 12px;
      height: 12px;
      -moz-border-radius: 7.5px;
      -webkit-border-radius: 7.5px;
      border-radius: 7.5px;
      background-color: ${({ theme }) => theme.colors.footerGray};
      margin-left: -0.35rem;
    }
  }
`
const Hea = styled.div`
  margin: 0.18rem 1rem 0.8rem 0;
`

function ExperienceBox({ item }) {
  const { gutters } = useContext(ThemeContext)

  // const router = useRouter()

  /*   const onClick = (url) => {
    router.push(`company/${url}`)
  } */

  return (
    <WrapperBoxItem id="work">
      <SchoolLogo /* onClick={() => onClick(item.company.id)} */>
        <Image
          src={item.company[0].logo}
          height={39}
          width={68}
          objectFit="contain"
        />
        <Text />
      </SchoolLogo>
      <SchoolDesc>
        <Hea>
          <BoldTextRegular
            //  onClick={() => openInNewTab(item.company.companyWebsite)}
            style={{
              textDecoration: 'underline',
              paddingTop: '5px',
              paddingBottom: '6px',
              cursor: 'pointer',
            }}
          >
            #{item.company[0]?.name}
          </BoldTextRegular>
          <TinyTxt>{item.company[0]?.companyAddress},</TinyTxt>
        </Hea>
        {item.role.map((role) => (
          <Expcount>
            {item.role.length > 1 && <div className="span" />}
            <div key={role?.description} style={{ marginBottom: gutters.medium }}>
              <BoldText>{role?.position}</BoldText>
              {/* <TinyTxt style={{ marginBottom: '7px' }}>{role.period},</TinyTxt> */}
              <TinyTxt
                style={{ lineHeight: '22px' }}
                dangerouslySetInnerHTML={{ __html: role?.description }}
              />{' '}
            </div>
          </Expcount>
        ))}
        {/*  <TinyTxt>{item.company.companyField}</TinyTxt> */}
      </SchoolDesc>
    </WrapperBoxItem>
  )
}

function WorkHistory() {
  const { gutters, colors } = useContext(ThemeContext)

  const [showAll, setShowAll] = useState(false)

  const toggleMore = () => {
    setShowAll(!showAll)
  }

  const data = [
    {
      company: [
        {
          name: 'Playback',
          companyAddress: 'Middletown | USA',
          logo: 'https://ucarecdn.com/72a1896b-6ae1-4190-8f9e-8a44220bc38e/',
          link: 'https://www.letsplayback.com/',
        },
      ],
      role: [
        {
          position: 'Mobile  React-native developer',
          description:
            '<ul><li>Developed and shipped highly interactive mobile application for Playback buyer platform using react native </li><li>Contributed extensively to Playback creator mail, a mail  that allows creators to organize their brand deals in ease</li><li>Worked closely with designer and management team to develop, and manage brand  website using Nextjs, styledcomponents, and JavaScript</li></ul>',
        },
        {
          position: 'Web  React/Nextjs developer',
          description:
            '<ul><li>Write modern, performant, and robust code for a diverse array of our internal web projects</li><li>Contributed extensively in development of world first video and virtual shopping platform a.k.a youtube for indie brands</li></li><li>Work with a variety of different languages, frameworks, and content management systems such as JavaScript,React,Nextjs,Graphcms</li><li>Communicate and collaborate with multi-disciplinary teams of engineers and designer</li></ul><p><br></p>',
        },
      ],
    },
    {
      company: [
        {
          name: 'Better coach',
          link: 'https://bettercoach.io/',
          companyAddress: 'Berlin | Germany',
          logo: 'https://ucarecdn.com/91368c87-698d-4242-af22-1c7c86f9c281/',
        },
      ],
      role: [
        {
          position: 'Frontend react reviewer',
          description:
            'Reviewed React/Typescript code quality working with a huge  universal code base. Monitored debugging process results and investigated causes of non-conforming software.Gave out some code recommedations that increased developers productivity',
        },
      ],
    },
    {
      company: [
        {
          name: 'Atilead',
          companyAddress: 'Nairobi | Kenya',
          logo: 'https://ucarecdn.com/f6ab0a3d-7839-44bf-838b-bfbd8bdee689/',
          link: 'https://atilead.com/',
        },
      ],
      role: [
        {
          position: 'Fullstack Javascript developer',
          description:
            '<ul><li>Revised, modularized and updated old code bases to modern development standards, reducing operating costs and improving functionality.<li>Collaborated on stages of systems development lifecycle from requirement gathering to production releases.</li></li></ul>',
        },
        {
          position: 'Web Frontend  Reactjs developer',
          description:
            '<ul><li>Worked with a  designer to build a marketing website and an e-commerce platform for Lapho,an ambitious venture originating from Sudan that boosted their conversion rate with over 40%</li><li>Interfaced with clients on a weekly basis, providing technological expertise and knowledge</li></ul>',
        },
      ],
    },
    {
      company: [
        {
          name: 'Node Tutorials',
          companyAddress: 'Uttar | India',
          logo: 'https://ucarecdn.com/7678340f-eeb4-428a-8ac8-91889cebcf2c/',
          link: 'https://nodejstutorials.net/',
        },
      ],
      role: [
        {
          position: 'Nodejs writer',
          description:
            'Wrote content on Express REST api not limited to auth flows,database access and security.Was responsible for the cms maintainance also',
        },
      ],
    },
  ]

  return (
    <Wrapper>
      <InnerWrapper>
        <WrapperBox>
          <Title style={{ marginBottom: gutters.small, marginTop: gutters.tiny }}>
            Where I’ve Worked
          </Title>
          <NormalText>
            Here is my past experiences of the places i have worked for both fulltime
            and freelance basis.
          </NormalText>
          {/*  {data.experience?.map((item) => (
        <ExperienceBox key={Math.random() * 6} item={item} />
      ))} */}
          {showAll
            ? data
                ?.slice(0, 10)
                ?.map((item) => (
                  <ExperienceBox key={Math.random() * 6} item={item} />
                ))
            : data
                ?.slice(0, 2)
                ?.map((item) => (
                  <ExperienceBox key={Math.random() * 6} item={item} />
                ))}
          {!showAll && data?.length > 2 && (
            <div
              onClick={() => toggleMore()}
              style={{ display: 'flex', alignItems: 'center', columnGap: '5px' }}
            >
              <NormalText style={{ color: colors.link, cursor: 'pointer' }}>
                See all
              </NormalText>
              <BsChevronCompactDown color={colors.link} fontSize={gutters.large} />
            </div>
          )}
        </WrapperBox>
      </InnerWrapper>
    </Wrapper>
  )
}

export default WorkHistory
