// import Image from 'next/image'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 1.5rem 1rem;
  overflow-y: auto;
  overflow-x: hidden;
`
const InnerWrapper = styled.div`
  width: 70vw;
  span {
    color: ${({ theme }) => theme.colors.textSlate};
    font-family: ${({ theme }) => theme.fonts.Apple};
    line-height: 24px;
    word-spacing: 3px;
    font-size: 22px;
  }
`

const Title = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.9rem;
  margin: 12px 0;
  font-weight: 600;
`
const Desc = styled.div`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: 24px;
  font-size: ${({ theme }) => theme.gutters.md};
  margin-top: 1.6rem;
  word-spacing: 4px;
  flex: 1;
`
const Tech = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  ul {
    li {
      font-size: ${({ theme }) => theme.gutters.md};
    }
  }
`
const DescImgWrapper = styled.div``
/* const ImageWrapper = styled.div`
  margin-top: 2rem;
  border-radius: 7px;
`
const Img = styled.div`
  height: 38vh;
  width: 38vw;
  position: relative;
  border-radius: 7px;
  transition: all 0.3s linear;
  &:hover {
    .flow {
      background: transparent;
    }
    .line {
      width: 18vw;
      right: 9rem;
      top: 0;
    }
  }

` */
/* const Line = styled.div`
  border: 3px solid ${({ theme }) => theme.colors.primary};
  height: 38vh;
  border-radius: 8px;
  width: 16.8vw;
  top: 39px;
  position: absolute;
  right: 8.2rem;
  transition: all 0.3s linear;
  cursor: pointer;
  &:hover {
    width: 18vw;
    right: 9rem;
    top: 0;
  }
` */

/* const Profile = styled.div`
  background: rgb(100, 255, 218, 0.3);
  height: 38vh;
  width: 38vw;
  position: absolute;
  width: 18vw;
  right: 9rem;
  top: 0;
  &:hover {
    background: transparent;
  }
` */

const index = () => {
  return (
    <Wrapper id="about">
      <InnerWrapper>
        <Title>Me in nutshell.</Title>
        <DescImgWrapper>
          <Desc>
            Hello! I enjoy creating things that live on the internet. My interest in
            web development started back in 2017 when I decided to try editing
            templates and the feeling was wow!
            <br />
            <br />
            Later i dived in coding dedicating most of my time .I learnt Energy and
            persistence conquers all things.I had started loving building stuffs that
            i could practically see.I fell in love with frontend side of product.Was
            an exciting one for me.
            <br />
            <br />
            Fast-forward to today,I’ve had the privilege of working at a creator
            economy company, a start-up, and an advertising agency. My main focus
            these days is building accessible, inclusive products and digital
            experiences at Playback.
            <br />
            {/* <br />I have recently launched a fully fledged saas ecommerce web app,
            <a
              style={{
                marginLeft: '0px',
                textDecoration: 'underline',
                color: 'rgb(100,255,218',
              }}
              href="https://letsplayback.com"
              target="_blank"
              rel="noreferrer"
            >
              Bebox
            </a>{' '}
            that describes the beauty and fashion related products.
            <br /> */}
            <br />
            Here are a few technologies I’ve been working with recently:
            <Tech>
              <ul>
                <li>JavaScript (ES6+)</li>
                <li>Typescript</li>
                <li>Reactjs</li>
                <li>Nextjs</li>
                <li>React native</li>
                <li>jest/supertest (TDD using unit test)</li>
                <li>Vercel/Aws-CI/CD provider with gitlab</li>
                <li>Appolo client(Graphql apis)</li>
              </ul>
              <ul>
                <li>Nodejs,Apollo-express-server Graphql api</li>
                <li>Aws amplify</li>
                <li>Firebase</li>
                <li>Production and the cloud(Docker+Aws)</li>
                <li>Sanity headless cms</li>
                <li>Aws services</li>
                <li>Redux/Reduxtoolkit</li>
              </ul>
            </Tech>
          </Desc>
          {/* <ImageWrapper>
            <Img>
              <Image
                style={{ overflow: 'hidden', borderRadius: '7px' }}
                objectFit="contain"
                src="https://ca.slack-edge.com/TAF02P92Q-U01HPKMA8BW-6b5451664044-512"
                layout="fill"
              />
              <Profile className="flow" />
              <Line className="line">
                <Image
                  style={{ opacity: 0, borderRadius: '7px' }}
                  objectFit="contain"
                  src="https://ca.slack-edge.com/TAF02P92Q-U01HPKMA8BW-6b5451664044-512"
                  layout="fill"
                />
              </Line>
            </Img>
          </ImageWrapper> */}
        </DescImgWrapper>
      </InnerWrapper>
    </Wrapper>
  )
}

export default index
