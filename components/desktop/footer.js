import { motion } from 'framer-motion'
import { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Wrapper = styled(motion.div)`
  background-color: ${({ theme }) => theme.colors.layout};
  display: flex;
  justify-content: center;
  margin-bottom: 2.1rem;
  /*  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%); */
`
const Title = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.white};
  font-size: 2.5rem;
  margin: 12px 0;
  font-weight: 600;
  text-align: center;
`
const CopyWrite = styled.p`
  font-family: 'Caveat', cursive;
  color: ${({ theme }) => theme.colors.textSlate};
  font-size: 24px;
  line-height: ${({ theme }) => theme.gutters.xxlarge};
`
const P = styled.p`
  text-align: center;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.textSlate};
  font-size: 16px;
  width: 40vw;
  line-height: 25px;
  justify-self: center;
`
const Wrap = styled.div``
const TalkButton = styled.button`
  background-color: ${({ theme }) => theme.colors.link};
  border: 1px solid transparent;
  margin-top: 2rem;

  &:hover {
    border: 0.5px solid ${({ theme }) => theme.colors.link};
    background-color: transparent;

    p {
      color: ${({ theme }) => theme.colors.link};
    }
  }
  p {
    color: ${({ theme }) => theme.colors.white};
    font-size: 17px;
  }
  outline: none;
  padding: 13px 40px;
  cursor: pointer;
  border-radius: 3px;
  transition: all 0.3s linear;
`

function Footer() {
  const { colors } = useContext(ThemeContext)
  return (
    <Wrapper
      id="contacts"
      initial={{ opacity: 0 }}
      transition={{ delay: 0.3 }}
      whileInView={{ opacity: 1 }}
      viewport={{ once: true }}
    >
      <div
        style={{
          width: '70vw',
          display: 'flex',
          alignItems: 'center',
          flexDirection: 'column',
        }}
      >
        <Title>Let&rsquo;s Connect</Title>
        <P>
          If you have an idea on product ,we can always discuss it and find a way
          through.Feel free to hit my inbox.Happy to help where i can!
        </P>
        <a
          href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=emmamwas99@gmail.com"
          target="_blank"
          rel="noreferrer"
          style={{ textDecoration: 'none', color: colors.textSlate }}
        >
          <TalkButton>
            <p>Say hi</p>
          </TalkButton>
        </a>

        <Wrap style={{ paddingTop: '1rem' }}>
          <CopyWrite>&#169; mwangi {new Date().getFullYear()}</CopyWrite>
        </Wrap>
      </div>
    </Wrapper>
  )
}

export default Footer
