// import Image from 'next/image'
import Image from 'next/image'
import styled, { ThemeContext } from 'styled-components'
import { BiLinkExternal } from 'react-icons/bi'
import { AiFillGitlab } from 'react-icons/ai'
import { useContext } from 'react'
import { openInNewTab } from '../../libs/helpers'

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: 2.6rem 1rem;
  overflow-y: auto;
  overflow-x: hidden;
`
const InnerWrapper = styled.div`
  width: 70vw;
  span {
    color: ${({ theme }) => theme.colors.textSlate};
    font-family: ${({ theme }) => theme.fonts.Apple};
    line-height: 24px;
    word-spacing: 3px;
    font-size: 22px;
  }
`

const Title = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.white};
  font-size: 1.9rem;
  margin: 12px 0;
  font-weight: 600;
`
const GridWrapper = styled.div`
  position: relative;
  height: 50vh;
  display: grid;
  grid-template-columns: 1fr 1fr;
  border-radius: 3px;
  margin-bottom: 6rem;
`
const ImageWrapper = styled.div`
  height: 100%;
  position: relative;
  background-color: #ffff;
`
const Name = styled.span`
  // background: ${({ theme }) => theme.colors.protip};
  background: rgba(69, 174, 202, 0.9);
  cursor: pointer;
  transition: all 0.3s linear;
  opacity: 0.2;
  &:hover {
    opacity: 0;
  }
  height: 100%;
  width: 100%;
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 3px;
  display: flex;
  align-items: center;
  justify-content: center;
`
const DescriptionWrapper = styled.div`
  background-color: transparent;
  height: 100%;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  position: relative;
`
const DescriptionBox = styled.div`
  background-color: rgb(17, 34, 64);
  width: 110%;
  // position: absolute;
  // left: -10%;
  padding: 1.1rem 1.5rem;
  border-radius: 3px;
`
const NormalText = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
  font-size: 15px;
  line-height: calc(24 / 16);
`
const ProjectName = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 600;
  font-size: 23px;
  line-height: calc(24 / 16);
  justify-self: flex-end;
  text-align: end;
  width: 100%;
`
const ProjectBoxWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 15px;
`
const ProjectBox = styled.div`
  background-color: ${({ theme }) => theme.colors.secondary};
  padding: 7px 12px 15px 12px;
`

const TechWrapper = styled.div`
  display: flex;
  column-gap: 12px;
  margin-top: 1rem;
  flex-wrap: wrap;
`
const Tech = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};

  -webkit-font-smoothing: antialiased;
  font-size: 15px;
  line-height: calc(24 / 16);
`
function Projects() {
  const { colors } = useContext(ThemeContext)

  const projects = [
    {
      name: 'Playback Mail App',
      projectImage: 'https://ucarecdn.com/c3310d91-8852-4112-bba7-ccee58da85b3/',
      description:
        'Email Reimagined For Creators.Still running your creator business on your personal email? Get Playback, the only business email app built for content creators',
      techStack: [
        'React',
        'React Native',
        'Typescript',
        'Ruby on rails Graphql api',
        'Redux/Reduxtoolkit',
      ],
      webLink: 'https://tikti.vercel.app/',
      playStoreUrl:
        'https://play.google.com/store/apps/details?id=com.playback.creator',
      linkUrl: 'https://creator.letsplayback.com/',
    },
    {
      name: 'Playback Video Shopping',
      projectImage: 'https://ucarecdn.com/95b4ccc6-b618-4d28-be99-16563ae66d3a/',
      description:
        'The world’s best video shopping marketplace for indie brand.Instantly buy the products your favorite creators love.What you get is what you see – always shipped for FREE.Join the video shopping revolution.',
      techStack: [
        'Reactjs',
        'Nextjs',
        'Appolo client',
        'Nodejs/express graphql api',
        'Ruby microservices',
        'Docker + aws',
      ],
      webLink: 'https://www.letsplayback.com/',
      linkUrl: 'https://www.letsplayback.com/',
    },
    {
      name: 'Creator Brand dashboard',
      projectImage: 'https://ucarecdn.com/f347d299-028d-483e-8a58-8924feb4ef7f/',
      description:
        'Manage your brand partnerships  with organized way through simple tools and flow  ...',
      techStack: [
        'Reactjs',
        'Nextjs',
        'Appolo client',
        'Ruby on rails Graphql api',
        'Docker + aws',
        'Typescript',
      ],
      webLink: 'https://brand.letsplayback.com/',
      linkUrl: 'https://brand.letsplayback.com/',
    },
    {
      name: 'Better coach',
      projectImage: 'https://ucarecdn.com/15e3dedf-1f65-40c6-81d7-58302ded195f/',
      description:
        'Global coach poolChoose from our global pool of 1,000+ rigorously selected certified coaches, facilitators, and experts. We always develop a dedicated coach pool for our clients to match your organizational and coaching requirements.',
      techStack: [
        'Reactjs',
        'Typescript',
        'Appolo client',
        'Rails Graphql api',
        'Aws amplify',
      ],
      webLink: 'https://bettercoach.io/',
      linkUrl: 'https://bettercoach.io/',
    },

    /*   {
      name: 'Instagram Clone',
      description: 'Post your best photos and let the worls comment',
      tech: ['React', 'Nextjs', 'Firebase', 'Recoil', 'Google Auth', 'Tailwind css'],
      gitlabLink: '',
      webLink: '',
    }, */
  ]

  const otherProjects = [
    {
      name: 'Creator Mobile storefront',
      description:
        'Create your storefront based on brands queries  and share the link with your  followers.Your followers buy what you like.Launch your online shop in minutes',
      tech: [
        'React',
        'Typescript',
        'Nextjs',
        'Nodejs Express api',
        'Tailwind css',
        'Styled components',
      ],
      webLink: 'https://letsplayback.com/',
    },
    /*   {
      name: 'Instagram Clone',
      description: 'Post your best photos and let the worls comment',
      tech: ['React', 'Nextjs', 'Firebase', 'Recoil', 'Google Auth', 'Tailwind css'],
      gitlabLink: '',
      webLink: '',
    }, */
    {
      name: 'Atilead',
      description:
        'Best customer management tools. Manage your leads centrally. Send bulk and automated marketing mails,',
      tech: [
        'Vuejs',
        'Nodejs/Express',
        'Graphql',
        'Docker+Aws',
        'Google Auth',
        'scss',
      ],
      webLink: 'https://atilead.com/',
    },
    {
      name: 'Admin,Playback',
      description:
        'A dashboard to manage all the brands and creators flow.It has the ability of creating campaigns,connecting resellers and more',
      tech: [
        'React',
        'Nextjs',
        'Firebase',
        'Ruby on rails graphql api',
        'styled components',
        'Aws',
      ],
      // webLink: 'https://ops.letsplayback.com/',
    },
    {
      name: 'Tiktok clone(for fun)',
      description:
        'View and add personalized short videos.Add videos of  and share the link with your network',
      tech: [
        'React',
        'Nextjs',
        'Typescript',
        'Sanityio',
        'Google Auth',
        'Tailwind css',
        'Styled components',
      ],
      gitlabLink: 'https://gitlab.com/chris_mwangi/tiktik.git',
      webLink: 'https://tikti.vercel.app/',
    },
    {
      name: 'Nodejs content writer',
      description:
        'Content on Express REST api not limited to auth flows and security.',
      tech: ['Nodejs', 'Graphcms', 'Rest', 'Google security'],
      webLink: 'https://nodejstutorials.net/node-js-rest-api-security-using-jwt/',
    },
    {
      name: 'Better coach',
      description:
        'Codebase review on the project.Solution for the (internal) coaching and workshops',
      tech: ['Ruby on rails api', 'Reactjs', 'Graphql', 'Typescript'],
      webLink: 'https://bettercoach.io/',
    },
  ]
  return (
    <Wrapper id="projects">
      <InnerWrapper>
        <Title>Recent Few Projects.</Title>
        {projects?.map((project) => (
          <GridWrapper>
            <ImageWrapper>
              <Image
                layout="fill"
                src={project?.projectImage}
                priority
                objectFit="contain"
              />
              <Name />
            </ImageWrapper>
            <DescriptionWrapper>
              <ProjectName>{project?.name}</ProjectName>
              <DescriptionBox>
                <NormalText>{project?.description}</NormalText>
              </DescriptionBox>
              <TechWrapper>
                {project?.techStack?.map((tech) => (
                  <Tech key={tech}>{tech}</Tech>
                ))}
              </TechWrapper>
              <div
                style={{
                  justifyContent: 'flex-end',
                  display: 'flex',
                  width: '100%',
                  columnGap: '13px',
                  cursor: 'pointer',
                }}
              >
                {project?.gitlabUrl && (
                  <AiFillGitlab
                    onClick={() => openInNewTab(project?.gitlabUrl)}
                    fontSize="24px"
                    color={colors.textSlate}
                  />
                )}
                <BiLinkExternal
                  onClick={() => openInNewTab(project?.linkUrl)}
                  fontSize="22px"
                  color={colors.textSlate}
                />
              </div>
            </DescriptionWrapper>
          </GridWrapper>
        ))}
        <Title
          style={{ textAlign: 'center', fontSize: '1.6rem', marginBottom: '1rem' }}
        >
          Other noteworthy projects
        </Title>
        <ProjectBoxWrapper>
          {otherProjects.map((pr) => (
            <ProjectBox>
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  marginBottom: '1rem',
                }}
              >
                {pr?.gitlabLink ? (
                  <AiFillGitlab
                    onClick={() => openInNewTab(pr?.gitlabLink)}
                    fontSize="28px"
                    color={colors.white}
                    style={{ cursor: 'pointer' }}
                  />
                ) : (
                  <div />
                )}
                {pr?.webLink && (
                  <BiLinkExternal
                    style={{ cursor: 'pointer' }}
                    onClick={() => openInNewTab(pr?.webLink)}
                    fontSize="28px"
                    color={colors.white}
                  />
                )}
              </div>
              <ProjectName style={{ textAlign: 'start', margin: 0 }}>
                {pr?.name}
              </ProjectName>

              <NormalText>{pr?.description}</NormalText>
              <TechWrapper>
                {pr?.tech?.map((tech) => (
                  <Tech key={tech}>{tech}</Tech>
                ))}
              </TechWrapper>
            </ProjectBox>
          ))}
        </ProjectBoxWrapper>
      </InnerWrapper>
    </Wrapper>
  )
}

export default Projects
