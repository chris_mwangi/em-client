import React from 'react'
import styled from 'styled-components'
import Header from './header'
import IntroSection from './intro'
import AboutMeSection from './about'
import Socials from './socials'
import WorkSection from './work'
import Projects from './projects'
import Footer from './footer'

const Wrapper = styled.div`
  background: ${({ theme }) => theme.colors.background};
  max-height: 100vh;
  overflow-x: hidden;
  overflow-y: auto;
  position: relative;
  // overflow-y: overlay;

  scrollbar-gutter: stable;
  &::-webkit-scrollbar {
    width: 8px;
  }

  &::-webkit-scrollbar-track {
    background: transparent;
    width: 8px;
  }

  &::-webkit-scrollbar-thumb {
    background: none;
    width: 8px;
    border-radius: 4px;
    transition-timing-function: linear;
    transition-property: background;
    transition-duration: 1.5s;
  }

  &:hover {
    &::-webkit-scrollbar-thumb {
      background: ${({ theme }) => theme.colors.link};
    }
  }
`
const NormalText = styled.p`
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin: 0.5em 0 1em;
  -webkit-font-smoothing: antialiased;
  font-weight: 400;
  font-size: 15px;
  line-height: calc(24 / 16);
  margin: 0 !important;
  text-align: center;
`
const D = styled.div`
  margin-bottom: 10px;
  .n {
    font-size: 16px;
    font-weight: 500;
  }
`
const BoldText = styled.p`
  color: ${({ theme }) => theme.colors.white};
  font-family: ${({ theme }) => theme.fonts.NunitoBold};
  font-size: 1.07rem;
  margin-bottom: 4px;
`
const Ref = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 2rem 0;
  display: none;
`

function Desktop() {
  return (
    <Wrapper>
      <Header />
      <IntroSection />
      <AboutMeSection />
      <Socials />
      <WorkSection />
      <Projects />
      <Ref>
        <BoldText>Referees</BoldText>
        <D>
          <NormalText className="n">Kannan Rhegu (CTO,Playback)</NormalText>
          <NormalText>kannan@letsplayback.com/+491734128500</NormalText>
        </D>
        <D>
          <NormalText className="n">
            Ajesh K Gopi(UI/UX designer,Playback)
          </NormalText>
          <NormalText>ajeshkgopi9566@gmail.com/+918111920413</NormalText>
        </D>
        <D>
          <NormalText className="n">
            Caxton Muthoni (Fullstack Engineer,Atilead)
          </NormalText>
          <NormalText>githinjicaxton323@gmail.com/+254743751575</NormalText>
        </D>
      </Ref>
      <Footer />
    </Wrapper>
  )
}

export default Desktop
