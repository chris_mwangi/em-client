import { useContext } from 'react'
import styled, { ThemeContext } from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  justify-content: center;
  padding: 2.6rem 1rem;
`
const InnerWrapper = styled.div`
  width: 70vw;
  span {
    color: rgb(204, 214, 246);
    font-family: ${({ theme }) => theme.fonts.Apple};
    line-height: 24px;
    word-spacing: 3px;
    font-size: 22px;
  }
`
const Intro = styled.p`
  color: ${({ theme }) => theme.colors.link};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: 24px;
  word-spacing: 3px;
  margin-bottom: 0.7rem;
`
/* const Name = styled.p`
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.white};
  font-weight: 900;
  font-size: 3.7rem;
  margin: 12px 0;
` */
const Do = styled.p`
  font-weight: 900;
  font-size: 3.6rem;
  font-family: ${({ theme }) => theme.fonts.Apple};
  margin-bottom: 12px;
  color: ${({ theme }) => theme.colors.white};
  margin-top: 12px;
`
const Desc = styled.p`
  width: 40vw;
  color: ${({ theme }) => theme.colors.textSlate};
  font-family: ${({ theme }) => theme.fonts.Apple};
  line-height: 27px;
  font-size: ${({ theme }) => theme.gutters.md};
  margin-top: 1.6rem;
  word-spacing: 4px;
`
const TalkButton = styled.button`
  background-color: ${({ theme }) => theme.colors.link};
  border: 1px solid transparent;
  margin-top: 2rem;

  &:hover {
    border: 0.5px solid ${({ theme }) => theme.colors.link};
    background-color: transparent;

    p {
      color: ${({ theme }) => theme.colors.link};
    }
  }
  p {
    color: ${({ theme }) => theme.colors.white};
    font-size: 17px;
  }
  outline: none;
  padding: 13px 40px;
  cursor: pointer;
  border-radius: 3px;
  transition: all 0.3s linear;
`

const index = () => {
  const { colors } = useContext(ThemeContext)
  return (
    <Wrapper>
      <InnerWrapper>
        <Intro>Hi there, i&rsquo;am</Intro>
        <span>EM Maina</span>
        {/* <Name>A MERNG developer</Name> */}
        <Do>I build websites and mobile apps.</Do>
        <Desc>
          Forward looking and detail-oriented software engineer with 4+ years
          background in creating and executing innovative software solutions to
          enhance business productivity. Highly experienced in all aspects of
          software development lifecycle and end-to-end project management from
          concept through to development and delivery.Recently, i have been focusing
          on building accessible, creator-centered products at{' '}
          <a
            style={{ color: 'rgb(100,255,218' }}
            href="https://letsplayback.com"
            target="_blank"
            rel="noreferrer"
          >
            Playback,
          </a>{' '}
          a business mail app,web dashboards,video streaming/shopping platform that
          gets what creators do.
        </Desc>

        <a
          href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=emmamwas99@gmail.com"
          target="_blank"
          rel="noreferrer"
          style={{ textDecoration: 'none', color: colors.textSlate }}
        >
          <TalkButton>
            <p>Let&rsquo;s connect</p>
          </TalkButton>
        </a>
      </InnerWrapper>
    </Wrapper>
  )
}

export default index
