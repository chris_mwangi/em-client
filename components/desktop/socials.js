import React, { useContext } from 'react'

import styled, { ThemeContext } from 'styled-components'
import { AiFillGitlab, AiFillLinkedin /*  AiOutlineTwitter */ } from 'react-icons/ai'
import { BiMailSend } from 'react-icons/bi'

const Wrapper = styled.div`
  position: fixed;
  left: 2.2rem;
  bottom: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  row-gap: 0.7rem;
  cursor: pointer;
`

const Line = styled.div`
  width: 1px;
  height: 5rem;
  background-color: ${({ theme }) => theme.colors.textSlate};
`

function Socials() {
  const { colors } = useContext(ThemeContext)
  const onClick = (url) => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  return (
    <Wrapper>
      <AiFillLinkedin
        onClick={() =>
          onClick('https://www.linkedin.com/in/mwangi-maina-6463281ab/')
        }
        fontSize="1.6rem"
        color={colors.textSlate}
      />
      <a
        href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=emmamwas99@gmail.com"
        target="_blank"
        rel="noreferrer"
        style={{ textDecoration: 'none', color: colors.textSlate }}
      >
        <BiMailSend fontSize="1.6rem" color={colors.textSlate} />
      </a>

      <AiFillGitlab
        onClick={() => onClick('https://gitlab.com/chris_mwangi')}
        fontSize="1.6rem"
        color={colors.textSlate}
      />
      {/*  <AiOutlineTwitter fontSize="1.6rem" color={colors.textSlate} /> */}
      <Line />
    </Wrapper>
  )
}

export default Socials
