import styled from 'styled-components'
import Image from 'next/image'

const Wrapper = styled.div`
  // border-bottom: 1px solid ${({ theme }) => theme.colors.border};
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(0px);
  -webkit-backdrop-filter: blur(0px);
  display: flex;
  justify-content: space-between;
  height: 4rem;
  display: flex;
  align-items: center;
  position: sticky;
  top: 0px;
  width: 100%;
  z-index: 100;
  background: ${({ theme }) => theme.colors.background};
  transition: top 0.2s ease-in-out;
  .hide {
    top: -24px;
  }
  .show {
    top: 0;
  }
`
const Wrap = styled.div`
  width: 94vw;
  justify-content: space-between;
  display: flex;
  align-items: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`
const Logo = styled.button`
  outline: none;
  border: none;
  border-radius: 10px;
  transition: all 0.3s linear;
  cursor: pointer;
  height: 40px;
  width: 50px;
  position: relative;
  &:hover {
    .text {
      background: rgba(253, 213, 79, 0.3);
    }
  }
`
const Text = styled.p`
  color: ${({ theme }) => theme.colors.secondary};
  font-family: 'Caveat', cursive;
  font-size: ${({ theme }) => theme.gutters.regular};
  line-height: ${({ theme }) => theme.gutters.xxxlarge};
  background: rgba(253, 213, 79, 0.1);
  overflow: hidden;
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  top: 0;
  left: 0;
  border-radius: 10px;
`
const List = styled.div`
  display: flex;
  font-family: ${({ theme }) => theme.fonts.Apple};
  font-size: ${({ theme }) => theme.gutters.medium};
  column-gap: ${({ theme }) => theme.gutters.large};
  padding: ${({ theme }) => theme.gutters.medium} 23px;
  align-items: center;
`
const ListItem = styled.div`
  cursor: pointer;
  border: 1px solid transparent;
  transition: all 0.3s linear;
  padding-bottom: 6px;
  &:hover {
    border-bottom: 1px solid ${({ theme }) => theme.colors.link};
  }
`
const ListText = styled.a`
  text-decoration: none;
  color: rgb(204, 214, 246);
  &:hover {
    color: ${({ theme }) => theme.colors.link};
  }
  transition: all 0.2s linear;
`
const ResumeButton = styled.button`
  border: 1px solid transparent;
  background-color: ${({ theme }) => theme.colors.link};

  &:hover {
    border: 1px solid ${({ theme }) => theme.colors.link};
    background-color: transparent;
    p {
      color: ${({ theme }) => theme.colors.link};
    }
  }
  p {
    color: ${({ theme }) => theme.colors.white};
  }
  outline: none;
  padding: 10px 18px;
  cursor: pointer;
  border-radius: 3px;
  transition: all 0.3s linear;
`

const index = () => {
  const onClick = (url) => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }
  return (
    <Wrapper>
      <Wrap>
        <Logo>
          <Image
            style={{ borderRadius: '10px' }}
            layout="fill"
            alt="mwangi"
            src="/assets/mobile/images/logo.jpeg"
          />
          <Text className="text" />
        </Logo>
        <List>
          <ListItem>
            <ListText href="#about">About</ListText>
          </ListItem>
          <ListItem>
            <ListText href="#projects">Projects</ListText>
          </ListItem>
          <ListItem>
            <ListText href="#about">Skills</ListText>
          </ListItem>
          <ListItem>
            <ListText href="#work">Works</ListText>
          </ListItem>
          <ListItem>
            <ListText href="#contacts">Contacts</ListText>
          </ListItem>
          <ResumeButton onClick={() => onClick('/assets/pdf/cv.pdf')}>
            <p>Resume</p>
          </ResumeButton>
        </List>
      </Wrap>
    </Wrapper>
  )
}

export default index
