/* eslint-disable no-unused-vars */
import { useRouter } from 'next/router'
import styled, { ThemeContext } from 'styled-components'
import { AiFillHome } from 'react-icons/ai'
import { IoServer, IoSearchCircle } from 'react-icons/io5'
import { useContext } from 'react'
import { BsFillChatDotsFill, BsFillPeopleFill } from 'react-icons/bs'

const MainContainer = styled.div`
  width: 100%;
  height: 55px;
  position: fixed;
  bottom: 0;
  z-index: 200;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  // border-top: 1px solid ${(props) => props.theme.colors.graySlight};
  background-color: ${({ theme }) => theme.colors.layout};
`
const NavigationContainer = styled.div`
  margin: auto;
  width: 100%;
  display: grid;
  grid-template-columns: ${(props) =>
    props.companyActive ? 'repeat(5,1fr)' : 'repeat(4,1fr)'};
  align-items: center;
  justify-content: center;
  height: 100%;
`

const NavigationItem = styled.button`
  outline: none;
  padding: 5px 0;
  border: none;
  cursor: pointer;
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: ${({ theme }) => theme.colors.layout};
  border-top: ${(props) =>
    props.active
      ? `1.5px solid  ${props.theme.colors.text}`
      : `1px solid ${props.theme.colors.graySlight}`};
  height: 100%;
  transition: all 0.3s linear;
`

const NavigationText = styled.div`
  margin-top: 5px;
  font-size: 11px;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${(props) =>
    props.isActive ? props.theme.colors.textFooter : props.theme.colors.footerGray};
  font-family: ${({ theme }) => theme.fonts.Apple};
`

const ImageContainer = styled.svg`
  height: 22px;
  width: 22px;
  color: ${(props) => props.theme.gray};
  transition: all 0.5s linear;
`
const ImageContainerActive = styled.svg`
  height: 24px;
  width: 24px;
  transition: all 0.5s linear;
`

function MobileFooter() {
  const router = useRouter()
  const { colors, gutters } = useContext(ThemeContext)

  const isAboutActive = router.pathname === '/'
  const isProjectActive =
    router.pathname === '/projects' || router.pathname === '/projects/[project]'
  const isDiscover =
    router.pathname === '/discover' || router.pathname === '/company/[id]'
  const isTestimonials = router.pathname === '/testimonials'
  const isChat = router.pathname === '/chat'

  const isCompany = router.pathname === '/company/[id]'

  const navigateTo = (route) => {
    router.push(route, undefined, { shallow: true })
  }

  return (
    <MainContainer>
      <NavigationContainer companyActive={isCompany}>
        <NavigationItem active={isAboutActive} onClick={() => navigateTo('/')}>
          {!isAboutActive && (
            <ImageContainer>
              <AiFillHome color={colors.footerGray} fontSize={gutters.xlarge} />
            </ImageContainer>
          )}
          {isAboutActive && (
            <ImageContainerActive>
              <AiFillHome color={colors.textFooter} fontSize={gutters.xlarge} />
            </ImageContainerActive>
          )}
          <NavigationText isActive={isAboutActive}>Home</NavigationText>
        </NavigationItem>
        <NavigationItem
          active={isProjectActive}
          onClick={() => navigateTo('/projects')}
        >
          {isProjectActive && (
            <ImageContainerActive>
              <IoServer color={colors.textFooter} fontSize={gutters.xlarge} />
            </ImageContainerActive>
          )}
          {!isProjectActive && (
            <ImageContainer>
              <IoServer color={colors.footerGray} fontSize={gutters.xlarge} />
            </ImageContainer>
          )}
          <NavigationText isActive={isProjectActive}>Projects</NavigationText>
        </NavigationItem>
        {isCompany && (
          <NavigationItem
            active={isDiscover}
            // onClick={() => navigateTo('/discover')}
          >
            {isDiscover && (
              <ImageContainerActive>
                <IoSearchCircle
                  color={colors.textFooter}
                  fontSize={gutters.xlarge}
                />
              </ImageContainerActive>
            )}
            {!isDiscover && (
              <ImageContainer>
                <IoSearchCircle
                  color={colors.footerGray}
                  fontSize={gutters.xlarge}
                />
              </ImageContainer>
            )}
            <NavigationText isActive={isDiscover}>Discover</NavigationText>
          </NavigationItem>
        )}

        <NavigationItem
          active={isTestimonials}
          onClick={() => navigateTo('/testimonials')}
        >
          {isTestimonials && (
            <ImageContainerActive>
              <BsFillPeopleFill
                color={colors.textFooter}
                fontSize={gutters.xlarge}
              />
            </ImageContainerActive>
          )}
          {!isTestimonials && (
            <ImageContainer>
              <BsFillPeopleFill
                color={colors.footerGray}
                fontSize={gutters.xlarge}
              />
            </ImageContainer>
          )}
          <NavigationText isActive={isTestimonials}>Referees</NavigationText>
        </NavigationItem>
        <NavigationItem active={isChat} onClick={() => navigateTo('/chat')}>
          {!isChat && (
            <ImageContainer>
              <BsFillChatDotsFill
                color={colors.footerGray}
                fontSize={gutters.large}
              />
            </ImageContainer>
          )}
          <NavigationText isActive={isChat}>messages</NavigationText>
        </NavigationItem>
      </NavigationContainer>
    </MainContainer>
  )
}

export { MobileFooter }
