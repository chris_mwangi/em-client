import { motion } from 'framer-motion'
import Image from 'next/image'
import styled, { ThemeContext } from 'styled-components'
import { BsFillMoonStarsFill, BsFillSunFill } from 'react-icons/bs'
/* import {
  import { BsFillMoonStarsFill, BsFillSunFill } from 'react-icons/bs'
  BsFillSunFill,
  BsFillMoonStarsFill,
  BsArrowLeftSquare,
} from 'react-icons/bs' */
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai'
import { useContext, useState } from 'react'
import Menu from '../../mobile/about/menu'
import { useGlobalContext } from '../../../context/provider'
import { CLOSE_MENU, LIGHT_THEME, OPEN_MENU } from '../../../constants'
/* import { useRouter } from 'next/router'
import Search from '../../mobile/common/inputs/search'
import { LIGHT_THEME, SEARCH_PROJECT } from '../../../constants' */
// import { useGlobalContext } from '../../../context/provider'

const HeaderWrapper = styled.div`
  height: 55px;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 9999;
  opacity: 0.95;
  background-color: ${({ theme }) => theme.colors.header};
  display: flex;
  justify-content: space-between;
  // border-bottom: 1px solid ${(props) => props.theme.colors.graySlight};
  transition: all 0.5s linear;
  align-items: center;
  align-content: center;
  padding-right: ${({ theme }) => theme.gutters.tiny};
  padding-left: ${({ theme }) => theme.gutters.tiny};
`

const DesktopWrapper = styled.div`
  height: 82px;
  position: fixed;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 500;
  background-color: rgba(${({ theme }) => theme.colors.textDark}, 0.95);
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid ${(props) => props.theme.colors.graySlight};
  align-items: center;
  align-content: center;
`

const Wrap = styled(motion.div)`
  display: flex;
  row-gap: 8px; /* ⇳ only */
  column-gap: 7px;
  align-items: center;
  margin: 0 0.6rem;
  transition: all 0.5s linear;
`
const Button = styled.button`
  outline: none;
  border: none;
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: transparent;
`

const LogoWrapper = styled.div``

/* const TextMsg = styled.p`
  font-size: ${({ theme }) => theme.gutters.large};
  font-weight: 600;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.text};
`
const TextName = styled.p`
  font-size: 18px;
  font-weight: 600;
  font-family: ${({ theme }) => theme.fonts.Apple};
  color: ${({ theme }) => theme.colors.text};
`

const MessageTextWrapper = styled.div`` */

function ImageContainer({ src, sm }) {
  return (
    <Image
      style={{ borderRadius: '12px' }}
      alt="em logo"
      src={src}
      height={sm ? 22 : 36}
      width={sm ? 22 : 36}
    />
  )
}

function MobileHeader({ theme, themeToggler }) {
  //  const { userState } = useGlobalContext()
  const { colors } = useContext(ThemeContext)
  const [showModal, setShowModal] = useState(false)
  const { menuDispatch } = useGlobalContext()

  // const router = useRouter()

  /*   const {
    pathname,
    query: { name },
  } = router

  const isDiscover = pathname === '/discover'

  const isChat = pathname === '/chat'

  const isDetailedProject = pathname === '/projects/[project]'

  const isCompany = pathname === '/company/[id]' */

  /*   if (isChat) {
    return (
      <HeaderWrapper>
        <LogoWrapper onClick={() => router.back()}>
          <BsArrowLeftSquare color={colors.footerGray} fontSize={gutters.large} />
        </LogoWrapper>
        <MessageTextWrapper>
          <TextMsg>Messages</TextMsg>
        </MessageTextWrapper>

        <div>
          <Button>
            {theme === LIGHT_THEME ? (
              <BsFillMoonStarsFill
                color={colors.footerGray}
                fontSize={gutters.regular}
                onClick={themeToggler}
              />
            ) : (
              <BsFillSunFill
                color={colors.footerGray}
                fontSize={gutters.regular}
                onClick={themeToggler}
              />
            )}
          </Button>
        </div>
      </HeaderWrapper>
    )
  } */

  /*   if (isDetailedProject || isCompany) {
    return (
      <HeaderWrapper>
        <LogoWrapper onClick={() => router.back()}>
          <BsArrowLeftSquare color={colors.footerGray} fontSize={gutters.large} />
        </LogoWrapper>
        <MessageTextWrapper>
          <TextName>{name}</TextName>
        </MessageTextWrapper>

        <div>
          <Button>
            {theme === LIGHT_THEME ? (
              <BsFillMoonStarsFill
                color={colors.footerGray}
                fontSize={gutters.regular}
                onClick={themeToggler}
              />
            ) : (
              <BsFillSunFill
                color={colors.footerGray}
                fontSize={gutters.regular}
                onClick={themeToggler}
              />
            )}
          </Button>
        </div>
      </HeaderWrapper>
    )
  } */
  return (
    <>
      <Menu showModal={showModal} setShowModal={setShowModal} />
      <HeaderWrapper>
        <LogoWrapper>
          <ImageContainer src="/assets/mobile/images/logo.jpeg" />
        </LogoWrapper>

        {/* {isDiscover && <Search placeholder="Search..." type={SEARCH_PROJECT} />} */}
        <Wrap
          initial={{ opacity: 0 }}
          transition={{ delay: 0.6 }}
          whileInView={{ opacity: 1 }}
          viewport={{ once: true }}
        >
          <div>
            <Button>
              {theme === LIGHT_THEME ? (
                <BsFillSunFill
                  color={colors.footerGray}
                  fontSize="19px"
                  onClick={themeToggler}
                />
              ) : (
                <BsFillMoonStarsFill
                  color={colors.footerGray}
                  fontSize="19px"
                  onClick={themeToggler}
                />
              )}
            </Button>
          </div>
          {showModal ? (
            <AiOutlineClose
              onClick={() => {
                menuDispatch({ type: CLOSE_MENU })
                setShowModal(!showModal)
              }}
              color={colors.white}
              fontSize="21px"
            />
          ) : (
            <AiOutlineMenu
              onClick={() => {
                menuDispatch({ type: OPEN_MENU })
                setShowModal(!showModal)
              }}
              color={colors.white}
              fontSize="24px"
            />
          )}
        </Wrap>
      </HeaderWrapper>
    </>
  )
}

function DesktopHeader() {
  return (
    <DesktopWrapper>
      <LogoWrapper>
        <ImageContainer src="/assets/mobile/images/logo.jpeg" />
      </LogoWrapper>
    </DesktopWrapper>
  )
}

export { MobileHeader, DesktopHeader }
