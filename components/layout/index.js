import styled from 'styled-components'
import { memo, useEffect, useState } from 'react'
// import useDetectKeyboardOpen from 'use-detect-keyboard-open'
import { isMobile } from 'react-device-detect'

// import { useRouter } from 'next/router'
import { MobileHeader } from './header'

// import { MobileFooter } from './footer'
import { useGlobalContext } from '../../context/provider'
// import { useGlobalContext } from '../../context/provider'

const Wrapper = styled.div`
  overflow-y: auto;
`

const index = ({ themeToggler, theme, children }) => {
  const [mounted, setMounted] = useState(false)
  const { loadingState } = useGlobalContext()

  // const { tabState } = useGlobalContext()
  /* const isKeyboardOpen = useDetectKeyboardOpen()

  const router = useRouter()
  const { pathname } = router

  const isChat = pathname === '/chat'
  const isDetailedProject = pathname === '/projects/[project]' */

  useEffect(() => {
    setMounted(true)
  })
  if (!mounted) {
    return null
  }
  if (!isMobile) {
    return children
  }
  return (
    <Wrapper>
      {!loadingState.openLoading && (
        <MobileHeader themeToggler={themeToggler} theme={theme} />
      )}

      {children}
      {/*  {!isKeyboardOpen && <MobileFooter /> &&
        !isChat && <MobileFooter /> &&
        !isDetailedProject && <MobileFooter /> &&
        !loadingState.openLoadingHome && <MobileFooter />} */}
    </Wrapper>
  )
}

export default memo(index)
