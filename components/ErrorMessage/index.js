/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable import/no-cycle */
import { motion } from 'framer-motion'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import { useCallback, useEffect, useRef } from 'react'
// import axios from 'axios'
import styled from 'styled-components'
import { BsFillExclamationCircleFill } from 'react-icons/bs'
import { CLOSE_ERROR } from '../../constants'
import { useGlobalContext } from '../../context/provider'

const Background = styled(motion.div)`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-content: center;
  align-items: center;
  z-index: 9999;
`

const ImageClose = styled.div`
  cursor: pointer;
  right: 5%;
  top: 5%;
  position: absolute;
`

const ModalWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: fit-content;
  height: fit-content;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  border-radius: 10px;
`

const ModalContent = styled.div`
  padding: 1rem;
  width: fit-content;
  height: fit-content;
  min-height: 300px;
  min-width: 300px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  text-align: center;
  h1 {
    font-size: 24px;
    font-family: Nunito-Black;
    margin-bottom: 0;
    margin-top: 0.5rem;
  }
  p {
    font-size: 16px;
    font-family: Nunito-Regular;
    line-height: 1.5rem;
  }
  h2 {
    font-size: 15px;
    font-family: Nunito-Regular;
    margin-bottom: 0;
    margin-top: 0.5rem;
    text-decoration: underline;
    margin-top: 23px;
  }
`

const Button = styled.button`
  outline: none;
  background-color: transparent;
  border: none;
  font-size: 15px;
  font-family: Nunito-Regular;
  margin-bottom: 0;
  margin-top: 0.5rem;
  text-decoration: underline;
  margin-top: 7px;
`

function ErrorMessage({ showModal, message }) {
  const { errorDispatch } = useGlobalContext()

  const modalRef = useRef()

  const closeModal = (e) => {
    if (modalRef.current === e.target) {
      errorDispatch({
        type: CLOSE_ERROR,
      })
    }
  }

  const keyPress = useCallback(
    (e) => {
      if (e.key === 'Escape' && showModal) {
        errorDispatch({
          type: CLOSE_ERROR,
        })
      }
    },
    [showModal]
  )

  useEffect(() => {
    document.addEventListener('keydown', keyPress)
    return () => document.removeEventListener('keydown', keyPress)
  }, [keyPress])

  const login = (url) => {
    window.open(url, '_self')
  }

  return (
    <>
      {showModal && (
        <Background
          onClick={closeModal}
          ref={modalRef}
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          exit={{ opacity: 0 }}
        >
          <ModalWrapper showModal={showModal}>
            <ImageClose
              onClick={() =>
                errorDispatch({
                  type: CLOSE_ERROR,
                })
              }
            >
              <AiOutlineCloseCircle fontSize="23px" color="#020202" />
            </ImageClose>
            <ModalContent>
              <BsFillExclamationCircleFill fontSize="40px" color="#ff4f6e" />
              <h1>Error</h1>
              <p>{message}</p>
              {message === 'please login' && (
                <Button
                  onClick={() => login(`${process.env.SERVER_URI}auth/google`)}
                  type="button"
                >
                  Login to continue
                </Button>
              )}
            </ModalContent>
          </ModalWrapper>
        </Background>
      )}
    </>
  )
}
// AnimatePresence
export default ErrorMessage
