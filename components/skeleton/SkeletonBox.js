import styled from 'styled-components'

const Skeleton = styled.div`
  display: inline-block;
  height: 100%;
  width: 100%;
  background: linear-gradient(-90deg, #dedede 0%, #e8e8e8 50%, #dedede 100%);
  background-size: 400% 400%;
  border-radius: 8px;
  animation: wave 1s ease-in-out infinite;
  margin: 0.3em 0;
  box-sizing: border-box;

  &::before {
    content: '\\00a0';
  }

  @keyframes wave {
    0% {
      background-position: 0% 0%;
    }
    100% {
      background-position: -135% 0%;
    }
  }
`

const SkeletonBox = ({ amount = 1 }) => {
  return new Array(amount).fill(0).map((_, i) => <Skeleton key={i} />)
}

export default SkeletonBox
