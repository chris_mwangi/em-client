function GeneralHead({ description, ogUrl, ogImage, ogTitle }) {
  return (
    <>
      <meta property="og:url" content={ogUrl} key="ogurl" />
      <meta property="og:image" content={ogImage} key="ogimage" />
      <meta property="og:site_name" content="Mwangi Maina" key="ogsitename" />
      <meta property="og:title" content={ogTitle} key="ogtitle" />
      <meta property="og:description" content={description} key="ogdesc" />
      <meta property="og:image:width" content="400" />
      <meta property="og:image:height" content="300" />
      <meta property="og:image:secure_url" content={ogImage} />
      <meta
        property="og:image:alt"
        content="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
      />
    </>
  )
}

export default GeneralHead
