import { useState, useEffect } from 'react'
import axios from 'axios'
// import { usersRoute } from '../utils/apiRoutes';

const useAxios = (url, method = 'get', data = null) => {
  const [response, setResponse] = useState(null)
  const [error, setError] = useState(null)
  const [loading, setLoading] = useState(false)
  const fetchData = async () => {
    setLoading(true)
    try {
      if (method === 'post') {
        const res = await axios.post(url, { data })
        setResponse(res.data)
        setError(null)
      } else {
        const res = await axios.get(url)
        setResponse(res.data)
        setError(null)
      }
    } catch (err) {
      setError(err)
    } finally {
      setLoading(false)
    }
  }
  useEffect(() => {
    fetchData()
  }, [])
  return { response, error, loading }
}
export default useAxios
