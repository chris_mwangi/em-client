/* eslint-disable import/no-unresolved */
import { ThemeProvider } from 'styled-components'
import Head from 'next/head'
import { ApolloProvider } from '@apollo/client'
import client from '../apollo'
import useTheme from '../hooks/useTheme'
import { lightTheme, darkTheme } from '../styles/theme'
import { LIGHT_THEME } from '../constants'
import { GlobalStyles } from '../styles/global'
import 'swiper/css/pagination'
import GlobalProvider from '../context/provider'
import Layout from '../components/layout'

import 'swiper/css'
import 'swiper/css/navigation'
// import GeneralHead from '../components/generaOgHead'
// import TwitterHead from '../components/twitterOgHead'

function MyApp({ Component, pageProps }) {
  const [theme, themeToggler] = useTheme()

  const selectedTheme = theme === LIGHT_THEME ? lightTheme : darkTheme

  return (
    <GlobalProvider>
      <Head lang="en">
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Fullstack javascript developer" />
        <meta charSet="utf-8" />
        <link
          href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
          rel="stylesheet"
        />

        {/*  <GeneralHead
          description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
          ogUrl="https://mwangimaina.com/"
          ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
          ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />

        <TwitterHead
          description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
                executing innovative software solutions."
          ogUrl="https://mwangimaina.com"
          ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
          ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        /> */}

        <title>Mwangi,React|Reactnative|Nextjs|Nodejs|Graphql developer</title>
        <link rel="manifest" href="/manifest.json" />
        <link
          rel="mask-icon"
          href="/assets/mobile/images/logo.jpeg"
          color="#5bbad5"
        />

        <meta name="application-name" content="Emaina" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="default" />
        <meta name="apple-mobile-web-app-title" content="Emaina" />
        <meta
          name="description"
          content="Forward looking and detail-oriented software engineer with 4+ years background in creating and executing innovative software solutions to enhance business productivity."
        />
        <meta name="format-detection" content="telephone=+254791608150" />
        <meta name="mobile-web-app-capable" content="yes" />
        <meta name="msapplication-config" content="/icons/browserconfig.xml" />
        <meta name="msapplication-TileColor" content="#2B5797" />
        <meta name="msapplication-tap-highlight" content="no" />
        <meta name="theme-color" content="rgb(17, 34, 64)" />

        <link rel="apple-touch-icon" href="/assets/mobile/images/logo.jpeg" />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="/assets/mobile/images/logo.jpeg"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/assets/mobile/images/logo.jpeg"
        />
        <link
          rel="apple-touch-icon"
          sizes="167x167"
          href="/assets/mobile/images/logo.jpeg"
        />

        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/assets/mobile/images/logo.jpeg"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/assets/mobile/images/logo.jpeg"
        />

        <link rel="shortcut icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"
        />

        <meta name="twitter:card" content="summary" />
        <meta name="twitter:url" content="https://mwangimaina.com" />
        <meta name="twitter:title" content="Emaina" />
        <meta
          name="twitter:description"
          content="Forward looking and detail-oriented software engineer with 4+ years background in creating and executing innovative software solutions to enhance business productivity."
        />
        <meta
          name="twitter:image"
          content="https://www.mwangimaina.com/assets/mobile/images/logo.jpeg"
        />
        <meta
          name="twitter:creator"
          content="Forward looking and detail-oriented software engineer with 4+ years background in creating and executing innovative software solutions to enhance business productivity."
        />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Emaina" />
        <meta
          property="og:description"
          c
          content="Forward looking and detail-oriented software engineer with 4+ years background in creating and executing innovative software solutions to enhance business productivity."
        />
        <meta property="og:site_name" content="Emaina" />
        <meta property="og:url" content="https://mwangimaina.com" />
        <meta
          property="og:image"
          content="https://www.mwangimaina.com/assets/mobile/images/logo.jpeg"
        />
      </Head>
      <ThemeProvider theme={selectedTheme}>
        <GlobalStyles />
        <ApolloProvider client={client}>
          <Layout themeToggler={themeToggler} theme={theme}>
            <Component themeToggler={themeToggler} theme={theme} {...pageProps} />
          </Layout>
        </ApolloProvider>
      </ThemeProvider>
    </GlobalProvider>
  )
}

export default MyApp
