import styled from 'styled-components'
import { isMobile } from 'react-device-detect'

import ClientOnly from '../../components/clientOnly'
import ProjectDetailed from '../../components/mobile/projects/projectdetailed'

const EntryWrapper = styled.div`
  margin-top: 55px;
`
const DesktopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  p {
    font-size: 25px;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`

const index = () => {
  if (isMobile) {
    return (
      <EntryWrapper>
        <ClientOnly>
          <ProjectDetailed />
        </ClientOnly>
      </EntryWrapper>
    )
  }
  return (
    <EntryWrapper>
      <DesktopWrapper>
        <p>Please use your mobile phone.</p>
      </DesktopWrapper>
    </EntryWrapper>
  )
}

export default index
