import Head from 'next/head'
import styled from 'styled-components'
import { isMobile } from 'react-device-detect'

/* import { gql } from '@apollo/client'
import client from '../../apollo' */
import Projects from '../../components/mobile/projects'
import ClientOnly from '../../components/clientOnly'

const EntryWrapper = styled.div`
  margin-top: 55px;
`
const DesktopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  p {
    font-size: 25px;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`

const index = () => {
  if (isMobile) {
    return (
      <EntryWrapper>
        <Head>
          <title>MERNG | projects recently worked on</title>
          <meta
            name="description"
            content="Merng stack developer.React,graphql,nextjs,nodejs"
          />
        </Head>
        <ClientOnly>
          <Projects />
        </ClientOnly>
      </EntryWrapper>
    )
  }
  return (
    <EntryWrapper>
      <Head>
        <title>MERNG | projects recently worked on</title>
        <meta
          name="description"
          content="Merng stack developer.React,graphql,nextjs,nodejs"
        />
      </Head>
      <DesktopWrapper>
        <p>Please use your mobile phone.</p>
      </DesktopWrapper>
    </EntryWrapper>
  )
}
/* export async function getStaticProps() {
  const { data } = await client.query({
    query: gql`
      query Getprojects {
        getProjects {
          projects {
            id
            projectTitle
            description
            isMobile
            projectImages
            company {
              id
              companyName
              companyLogo
              companyAddress
              companyWebsite
            }
            projectlikes {
              googleId
              userName
              profilePicUrl
              createdAt
            }
            comments {
              body
              userName
              profilePicUrl
              createdAt
              likes {
                userName
                profilePicUrl
              }
            }

            techStack {
              id
              techName
              techLink
            }
          }
          errors {
            key
            message
          }
          response {
            status
            code
          }
        }
      }
    `,
  }) */
/*   return {
    props: {
      data,
    },

    revalidate: 10,
  }
} */

export default index
