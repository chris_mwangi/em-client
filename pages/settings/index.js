import styled from 'styled-components'

const EntryWrapper = styled.div`
  margin-top: 55px;
`

const index = () => {
  return <EntryWrapper>preferences</EntryWrapper>
}

export default index
