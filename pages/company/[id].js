import React from 'react'
import styled from 'styled-components'
import { isMobile } from 'react-device-detect'

import Company from '../../components/mobile/company'

const EntryWrapper = styled.div`
  margin-top: 55px;
`
const DesktopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  p {
    font-size: 25px;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`

function Com() {
  if (isMobile) {
    return (
      <EntryWrapper>
        <Company />
      </EntryWrapper>
    )
  }
  return (
    <EntryWrapper>
      <DesktopWrapper>
        <p>Please use your mobile phone.</p>
      </DesktopWrapper>
    </EntryWrapper>
  )
}

export default Com
