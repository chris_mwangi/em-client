import styled from 'styled-components'
import { isMobile } from 'react-device-detect'
import Head from 'next/head'
import Mobile from '../components/mobile/about'
import Desktop from '../components/desktop'
import GeneralHead from '../components/generaOgHead'
import TwitterHead from '../components/twitterOgHead'
// import InLoader from '../components/loaderInitial/inLoader'

const Main = styled.div`
  background: ${({ theme }) => theme.colors.background};

  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`

const PageEntryWrapper = styled.div`
  background: ${({ theme }) => theme.colors.background};

  &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    background: transparent;
    width: 0px;
  }
`

const DesktopWrapper = styled.div`
  background: ${({ theme }) => theme.colors.background};
`

export default function Home({ themeToggler, theme }) {
  // If the page is not yet generated, this will be displayed
  // initially until getStaticProps() finishes running

  if (isMobile) {
    return (
      <PageEntryWrapper>
        {/* default deets */}
        <Head lang="en">
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta name="description" content="Fullstack javascript developer" />
          <meta charSet="utf-8" />
          <link
            href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
            rel="stylesheet"
          />

          {/* OG Sharing Deets */}
          <GeneralHead
            description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
            ogUrl="https://mwangimaina.com/"
            ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
            ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
          />

          {/* Twitter Sharing Deets */}
          <TwitterHead
            description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
                executing innovative software solutions."
            ogUrl="https://mwangimaina.com"
            ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
            ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
          />
          {/* regular title */}
          <title>Mwangi,React|Reactnative|Nextjs|Nodejs|Graphql developer</title>
        </Head>
        <Main>
          <Mobile themeToggler={themeToggler} theme={theme} />
        </Main>
      </PageEntryWrapper>
    )
  }
  return (
    <PageEntryWrapper>
      <Head lang="en">
        <title>React|Reactnative|Nextjs|Nodejs|Graphql developer</title>
        <meta
          name="title"
          content="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />
        <meta
          name="description"
          content="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
        />

        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://www.mwangimaina.com/" />
        <meta
          property="og:title"
          content="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />
        <meta
          property="og:description"
          content="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
        />
        <meta
          property="og:image"
          content="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
        />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://www.mwangimaina.com/" />
        <meta
          property="twitter:title"
          content="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />
        <meta
          property="twitter:description"
          content="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
        />
        <meta
          property="twitter:image"
          content="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
        />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Fullstack javascript developer" />
        <meta charSet="utf-8" />
        <link
          href="https://fonts.googleapis.com/css2?family=Caveat:wght@700&display=swap"
          rel="stylesheet"
        />

        {/* OG Sharing Deets */}
        <GeneralHead
          description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
            executing innovative software solutions."
          ogUrl="https://mwangimaina.com/"
          ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
          ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />

        {/* Twitter Sharing Deets */}
        <TwitterHead
          description="Forward looking and detail-oriented Software engineer with 4+ years background in creating and
                executing innovative software solutions."
          ogUrl="https://mwangimaina.com"
          ogImage="https://ucarecdn.com/b0d30ebd-4585-4066-9201-90939da517ec/"
          ogTitle="React|Reactnative|Nextjs|Nodejs|Graphql  developer"
        />
        {/* regular title */}
        <title>Mwangi,React|Reactnative|Nextjs|Nodejs|Graphql developer</title>
      </Head>
      <DesktopWrapper>
        <Desktop />
      </DesktopWrapper>
    </PageEntryWrapper>
  )
}
