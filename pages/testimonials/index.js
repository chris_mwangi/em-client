import { isMobile } from 'react-device-detect'
import styled from 'styled-components'
import Refereers from '../../components/mobile/refereeres'

const DesktopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  p {
    font-size: 25px;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`

function index() {
  if (!isMobile) {
    return (
      <DesktopWrapper>
        <p>Please use your mobile phone.</p>
      </DesktopWrapper>
    )
  }
  return <Refereers />
}

export default index
