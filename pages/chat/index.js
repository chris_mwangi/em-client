// import { gql } from '@apollo/client'
import { memo } from 'react'
import { isMobile } from 'react-device-detect'
import styled from 'styled-components'
// import client from '../../apollo'
import Chat from '../../components/mobile/chat'

const DesktopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90vh;
  p {
    font-size: 25px;
    font-family: ${({ theme }) => theme.fonts.Apple};
  }
`
const EntryWrapper = styled.div`
  margin-top: 65px;
`

function index() {
  if (!isMobile) {
    return (
      <DesktopWrapper>
        <p>Please use your mobile phone</p>
      </DesktopWrapper>
    )
  }
  return (
    <EntryWrapper>
      <Chat />
    </EntryWrapper>
  )
}

export default memo(index)

/* export async function getServerProps() {
  const { data } = await client.query({
    query: gql`
      query etMessages {
        getUserEducation {
          getEducation {
            id
            courseName
            departmentName
            school {
              websiteLink
              name
              logoUrl
            }
          }
          errors {
            key
            message
          }
          response {
            status
            code
          }
        }
      }
    `,
  })
} */
