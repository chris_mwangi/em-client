import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: 'Nunito-Black';
    src: url('/assets/fonts/Nunito/Nunito-Black.ttf') format('truetype');
  }

  @font-face {
    font-family: 'Nunito-Regular';
    src: url('/assets/fonts/Nunito/Nunito-Regular.ttf') format('truetype');
  }

  @font-face {
    font-family: 'Nunito-SemiBold';
    src: url('/assets/fonts/Nunito/Nunito-SemiBold.ttf') format('truetype');
  }

  @font-face {
    font-family: 'Nunito-Bold';
    src: url('/assets/fonts/Nunito/Nunito-Bold.ttf') format('truetype');
  }

  * {
    box-sizing: border-box;
  }


   body {
     background: ${({ theme }) => theme.colors.background};
     color: ${({ theme }) => theme.color};
     margin: 0;
     -webkit-tap-highlight-color: transparent;
     p{
       margin: 0 ;
     }
  }
   


  .swiper-button-next,
  .swiper-button-prev {
  color: rgb(32, 110, 233)!important;
  background-color: rgba(255, 255, 255, 0.85) !important;
  box-shadow: 3px !important;
  padding: 5px !important;
  border-radius: 50% !important;
  z-index: 100;
  box-shadow: #ccc !important;
  box-shadow: 0px 0px 9px 1px #ccc !important;
  width: 60px !important;
  height: 60px !important;
}
.swiper-button-next::after,
.swiper-button-prev::after {
  font-size: 20px !important;
}
.swiper-button-disabled {
  display: none !important;
}
.swiper-pagination-bullet {
  background: rgb(32, 110, 233)!important;
  opacity: 0.3 !important;
  display: inline block !important;
}
.swiper-pagination-bullet-active {
   background: rgb(32, 110, 233)!important;
  opacity: 1 !important;
  display: inline block !important;
}
`
