export const Fonts = {
  NunitoBold: 'Nunito-Bold',
  NunitoSemibold: 'Nunito-SemiBold',
  NunitoRegular: 'Nunito-Regular',
  NunitoBlack: 'Nunito-Black',
  Appleugug: 'Nunito-Regular',
  Applesh:
    'Larsseit, Inter var, -apple-system, BlinkMacSystemFont, Segoe UI,Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,sans-serif;',
  Apple:
    'Larsseit, Inter var, -apple-system, BlinkMacSystemFont, Segoe UI,Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,sans-serif;',
}
