export const Gutters = {
  tiny: '8px',
  xsmall: '10px',
  small: '12px',
  medium: '15px',
  regular: '17px',
  large: '20px',
  xlarge: '22px',
  xxlarge: '27px',
  xxxlarge: '32px',
}
