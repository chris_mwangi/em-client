import { Colors } from './variables'
import { Fonts } from './fonts'
import { Gutters } from './gutters'
// import { Images } from './images'

const colorsLight = {
  primary: Colors.primary,
  background: Colors.background,
  secondary: Colors.secondary,
  tertiary: Colors.tertiary,
  text: Colors.text,
  layout: Colors.layout,
  graySlight: Colors.graySlight,
  primarySlight: Colors.primarySlight,
  footerGray: Colors.footerGray,
  link: Colors.link,
  textFooter: Colors.textFooter,
  chatBg: Colors.chatBg,
  inputBg: Colors.inputBg,
  header: Colors.header,
  white: Colors.white,
  textSlate: Colors.textSlate,
  banner: Colors.banner,
  lightestSlate: Colors.lightestSlate,
  protip: Colors.proTip,
  bg: Colors.bgLight,
}

const colorsDark = {
  primary: Colors.primaryDark,
  background: Colors.backgroundDark,
  secondary: Colors.secondaryDark,
  tertiary: Colors.tertiaryDark,
  text: Colors.textDark,
  layout: Colors.layoutDark,
  graySlight: Colors.graySlightDark,
  primarySlight: Colors.primarySlightDark,
  footerGray: Colors.footerGrayDark,
  link: Colors.link,
  textFooter: Colors.textFooterDark,
  chatBg: Colors.chatBgDark,
  inputBg: Colors.inputBgDark,
  header: Colors.headerDark,
  white: Colors.whiteDark,
  textSlate: Colors.textSlateDark,
  banner: Colors.bannerDark,
  lightestSlate: Colors.lightestDarkSlate,
  protip: Colors.proTipDark,
  bg: Colors.bgDark,
}

const gutters = {
  small: Gutters.small,
  tiny: Gutters.tiny,
  medium: Gutters.medium,
  regular: Gutters.regular,
  large: Gutters.large,
  xlarge: Gutters.xlarge,
  xxlarge: Gutters.xxlarge,
  xxxlarge: Gutters.xxxlarge,
  xsmall: Gutters.xsmall,
}

const fonts = {
  NunitoBold: Fonts.NunitoBold,
  NunitoSemibold: Fonts.NunitoSemibold,
  NunitoRegular: Fonts.NunitoRegular,
  NunitoBlack: Fonts.NunitoBlack,
  Apple: Fonts.Apple,
}

export const lightTheme = {
  colors: colorsLight,
  gutters,
  fonts,
}

export const darkTheme = {
  colors: colorsDark,
  gutters,
  fonts,
}
