/* eslint-disable import/no-cycle */
import { createContext, useReducer, useContext, useMemo } from 'react'
import dynamic from 'next/dynamic'

//  Here we import the initial states
import { user } from './initialStates/userState'
import { search } from './initialStates/searchState'
import { tab } from './initialStates/tabState'
import { loading } from './initialStates/loadingState'
import { error } from './initialStates/errorState'
import { menu } from './initialStates/menuState'

// Here we import the actions that changes our state incase of anything(dispatches)
import userReducer from './reducers/userReducer'
import searchReducer from './reducers/searchReducer'
import tabReducer from './reducers/tabReducer'
import loadingReducer from './reducers/loadingReducer'
import errorReducer from './reducers/errorReducer'
import menuReducer from './reducers/menuReducer'

// Common components
const ErrorMessage = dynamic(() => import('../components/ErrorMessage'), {
  ssr: false,
})

/* const LoaderSpinner = dynamic(() => import('../components/LoaderSpinner'), {
  ssr: false,
}) */

const InLoader = dynamic(() => import('../components/loaderInitial/inLoader'), {
  ssr: false,
})

// This the function that provides the context for the whole app(wrapper),we have to export it in order to use it anywhere down  our wrapped react tree
const GlobalContext = createContext({})
export const useGlobalContext = () => {
  return useContext(GlobalContext)
}

function GlobalProvider({ children }) {
  const [userState, userDispatch] = useReducer(userReducer, user)
  const [searchState, searchDispatch] = useReducer(searchReducer, search)
  const [tabState, tabDispatch] = useReducer(tabReducer, tab)
  const [loadingState, loadingDispatch] = useReducer(loadingReducer, loading)
  const [errorState, errorDispatch] = useReducer(errorReducer, error)
  const [menuState, menuDispatch] = useReducer(menuReducer, menu)

  const states = useMemo(
    () => ({
      userState,
      userDispatch,
      searchState,
      searchDispatch,
      tabState,
      tabDispatch,
      loadingState,
      loadingDispatch,
      errorState,
      errorDispatch,
      menuState,
      menuDispatch,
    }),
    [userState, searchState, tabState, loadingState, errorState, menuState]
  )

  return (
    <GlobalContext.Provider value={states}>
      {children}
      {errorState.openError && (
        <ErrorMessage
          key="error-message-modal"
          message={errorState.errorMessage}
          showModal={errorState.openError}
        />
      )}
      {loadingState.openLoading && (
        <InLoader key="loading-spinner-modal" showModal={loadingState.openLoading} />
      )}
    </GlobalContext.Provider>
  )
}

export default GlobalProvider
