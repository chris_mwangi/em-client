import { OPEN_MENU, CLOSE_MENU } from '../../constants'

const menu = (state, { type }) => {
  switch (type) {
    case OPEN_MENU:
      return {
        ...state,
        open: true,
      }
    case CLOSE_MENU:
      return {
        ...state,
        open: false,
      }

    default:
      return state
  }
}

export default menu
