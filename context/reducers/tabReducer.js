import { SET_TAB } from '../../constants'

const tab = (state, { type, payload }) => {
  switch (type) {
    case SET_TAB:
      return {
        ...state,
        showTab: payload,
      }

    default:
      return state
  }
}

export default tab
