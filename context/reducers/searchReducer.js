import { SEARCH_PROJECT, SEARCH_USER, SEARCH_MESSAGES } from '../../constants'

const search = (state, { type, payload }) => {
  switch (type) {
    case SEARCH_PROJECT:
      return {
        ...state,
        projectQueryString: payload,
      }
    case SEARCH_USER:
      return {
        ...state,
        usersQueryString: null,
      }
    case SEARCH_MESSAGES:
      return {
        ...state,
        messagesQueryString: null,
      }

    default:
      return state
  }
}

export default search
