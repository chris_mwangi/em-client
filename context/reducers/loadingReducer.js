import {
  OPEN_LOADING,
  CLOSE_LOADING,
  CLOSE_HOME_LOADING,
  OPEN_HOME_LOADING,
} from '../../constants'

const loading = (state, { type }) => {
  switch (type) {
    case OPEN_LOADING:
      return {
        ...state,
        openLoading: true,
      }

    case CLOSE_LOADING:
      return {
        ...state,
        openLoading: false,
      }
    case OPEN_HOME_LOADING:
      return {
        ...state,
        openLoadingHome: true,
      }
    case CLOSE_HOME_LOADING:
      return {
        ...state,
        openLoadingHome: false,
      }

    default:
      return state
  }
}

export default loading
