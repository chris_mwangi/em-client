import { gql } from '@apollo/client'

export const COMMENT_PROJECT = gql`
  mutation ($projectId: ID!, $body: String!, $assetUrl: String) {
    createComment(projectId: $projectId, body: $body, assetUrl: $assetUrl) {
      projects {
        id
        comments {
          id
          body
          userName
          profilePicUrl
          createdAt
          replies {
            id
            body
            googleId
            userName
            profilePicUrl
            createdAt
            likes {
              googleId
              userName
              profilePicUrl
            }
          }
          likes {
            googleId
            userName
            profilePicUrl
          }
        }
        projectlikes {
          userName
          profilePicUrl
          createdAt
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`

export const COMMENT_REPLY_PROJECT = gql`
  mutation ($projectId: ID!, $commentId: ID!, $body: String!, $assetUrl: String) {
    createCommentReply(
      projectId: $projectId
      commentId: $commentId
      body: $body
      assetUrl: $assetUrl
    ) {
      projects {
        id
        comments {
          id
          body
          userName
          profilePicUrl
          createdAt
          replies {
            id
            body
            googleId
            userName
            profilePicUrl
            createdAt
            likes {
              googleId
              userName
              profilePicUrl
            }
          }
          likes {
            googleId
            userName
            profilePicUrl
          }
        }
        projectlikes {
          userName
          profilePicUrl
          createdAt
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
