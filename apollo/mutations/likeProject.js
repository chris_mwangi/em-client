import { gql } from '@apollo/client'

export const LIKE_PROJECT = gql`
  mutation ($projectId: ID!) {
    likeProject(projectId: $projectId) {
      projects {
        id
        projectlikes {
          userName
          profilePicUrl
          createdAt
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
