import { gql } from '@apollo/client'

export const LIKE_COMMENT = gql`
  mutation ($projectId: ID!, $commentId: ID!) {
    likeComment(projectId: $projectId, commentId: $commentId) {
      projects {
        id
        projectlikes {
          userName
          profilePicUrl
          createdAt
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`

export const LIKE_COMMENT_REPLY = gql`
  mutation ($projectId: ID!, $commentId: ID!, $replyId: ID!) {
    likeCommentReply(
      projectId: $projectId
      commentId: $commentId
      replyId: $replyId
    ) {
      projects {
        id
        projectlikes {
          userName
          profilePicUrl
          createdAt
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
