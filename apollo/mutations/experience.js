import { gql } from '@apollo/client'

export const UPDATE_EXPIRIENCE = gql`
  mutation ($id: ID!, $period: String) {
    updateExperience(id: $id, period: $period) {
      experience {
        id
        period
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
