import { gql } from '@apollo/client'

export const CREATE_PROJECT = gql`
  mutation (
    $projectTitle: String
    $company: String
    $description: String
    $position: Float
    $detailedDescription: String
    $isMobile: Boolean
    $role: String
    $linkUrl: String
    $projectLength: String
    $projectVideos: [String]
    $projectImages: [String]
    $techStack: [String]
  ) {
    createProject(
      projectTitle: $projectTitle
      company: $company
      description: $description
      position: $position
      detailedDescription: $detailedDescription
      isMobile: $isMobile
      role: $role
      linkUrl: $linkUrl
      projectLength: $projectLength
      projectVideos: $projectVideos
      projectImages: $projectImages
      techStack: $techStack
    ) {
      projects {
        id
        projectTitle
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
