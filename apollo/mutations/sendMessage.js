import { gql } from '@apollo/client'

export const SEND_MESSAGE = gql`
  mutation ($to: ID!, $message: String!, $imageUrl: String) {
    sendMessage(to: $to, message: $message, imageUrl: $imageUrl) {
      messages {
        id
        imageUrl
        message
        sender {
          id
          displayName
          image
        }
        recipient {
          id
          displayName
          image
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
