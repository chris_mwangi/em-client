import { gql } from '@apollo/client'

export const GET_COMPANIES = gql`
  query {
    getCompanies {
      companies {
        id
        companyName
        companyLogo
        companyField
        companyAddress
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
export const GET_COMPANY = gql`
  query getCompany($companyId: ID!) {
    getCompany(companyId: $companyId) {
      companies {
        id
        companyName
        companyLogo
        companyCoverImage
        companyField
        companyAddress
        companyAbout
        companySize
        companyWebsite
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
