import { gql } from '@apollo/client'

export const GET_MESSAGES = gql`
  query getMessages($from: ID!) {
    getMessages(from: $from) {
      messages {
        id
        imageUrl
        message
        sender {
          id
          displayName
          image
        }
        recipient {
          id
          displayName
          image
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
