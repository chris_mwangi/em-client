import { gql } from '@apollo/client'

export const GET_USER_EDUCATION = gql`
  query {
    getUserEducation {
      getEducation {
        id
        courseName
        school {
          websiteLink
          name
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
