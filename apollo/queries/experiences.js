import { gql } from '@apollo/client'

export const GET_EXPIRIENCES = gql`
  query {
    getExperiences {
      experience {
        company {
          id
          companyName
          companyLogo
          companyWebsite
          companyAbout
        }
        role {
          position
          period
          description
        }
        period
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
