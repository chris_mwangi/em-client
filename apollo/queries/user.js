import { gql } from '@apollo/client'

export const GET_LOGGED_IN_USER = gql`
  query {
    getLoggedInUser {
      user {
        id
        googleId
        displayName
        firstName
        lastName
        email
        image
      }
      errors {
        key
        message
      }
    }
  }
`
