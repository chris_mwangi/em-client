import { gql } from '@apollo/client'

export const GET_PROJECTS = gql`
  query {
    getProjects {
      projects {
        id
        projectTitle
        description
        isMobile
        projectImages
        playstoreUrl
        appstoreUrl
        linkUrl
        techStack {
          id
          techName
          techLink
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`

export const FIND_MY_SINGLE_PROJECT = gql`
  query getProject($projectId: ID!) {
    getProject(projectId: $projectId) {
      projects {
        id
        projectTitle
        description
        position
        detailedDescription
        isMobile
        role
        linkUrl
        projectLength
        projectVideos
        projectImages
        company {
          id
          companyName
          companyLogo
          companyAddress
          companyWebsite
        }
        projectlikes {
          googleId
          userName
          profilePicUrl
          createdAt
        }
        playstoreUrl
        appstoreUrl
        comments {
          id
          body
          userName
          profilePicUrl
          createdAt
          assetUrl
          replies {
            id
            body
            googleId
            userName
            profilePicUrl
            createdAt
            assetUrl
            likes {
              googleId
              userName
              profilePicUrl
            }
          }
          likes {
            googleId
            userName
            profilePicUrl
          }
        }

        techStack {
          id
          techName
          techLink
        }
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
