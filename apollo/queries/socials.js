import { gql } from '@apollo/client'

export const GET_LOGGED_IN_USER = gql`
  query getSocial {
    getSocials {
      socials {
        id
        socialName
        socialProfileDescription
        socialLink
        socialIconUrl
      }
      errors {
        key
        message
      }
      response {
        status
        code
      }
    }
  }
`
