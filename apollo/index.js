// ./apollo-client.js

import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client'

const link = createHttpLink({
  uri: process.env.API_URI,
  credentials: 'include',
})

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
})

/* import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client'

const client = new ApolloClient({
  ssrMode: true,
  link: createHttpLink({
    uri: process.env.API_URI,
    credentials: 'include',
  }),
  cache: new InMemoryCache(),
}) */

export default client
